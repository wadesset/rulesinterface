﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class AccessGroupBox
    {
        [Key]
        public Guid Id { get; set; }
        public List<AccessGroup> AccessGroups { get; set; }
    }
}
