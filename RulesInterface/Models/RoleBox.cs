﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class RoleBox
    {
        [Key]
        public Guid Id { get; set; }
        public List<Role> Roles { get; set; }
    }
}
