﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class OrganizationBox
    {
        [Key]
        public Guid Id { get; set; }
        public List<Organization> Organizations { get; set; }
    }
}
