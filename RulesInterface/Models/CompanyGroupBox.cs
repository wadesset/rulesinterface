﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class CompanyGroupBox
    {
        [Key]
        public Guid Id { get; set; }
        public List<CompanyGroup> CompanyGroups { get; set; }
    }
}
