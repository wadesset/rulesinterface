﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Organization
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
