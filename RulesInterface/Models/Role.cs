﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Role
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
