﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class CompanyGroup
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
