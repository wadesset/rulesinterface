﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class UnitBox
    {
        [Key]
        public Guid Id { get; set; }
        public List<Unit> Units { get; set; }
    }
}
