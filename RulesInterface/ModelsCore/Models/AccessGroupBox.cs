﻿using System;
using System.ComponentModel.DataAnnotations;
using ModelsCore.Interfaces;

namespace ModelsCore.Models
{
    public class AccessGroupBox : IBaseBoxModel
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ParentBoxId { get; set; }
        public Guid SelectedValueId { get; set; }
        public string Name { get; set; }
    }
}
