﻿using System;

namespace ModelsCore.Interfaces
{
    public interface IBaseBoxModel
    {
        Guid Id { get; set; }
        Guid ParentBoxId { get; set; }
        Guid SelectedValueId { get; set; }
    }
}
