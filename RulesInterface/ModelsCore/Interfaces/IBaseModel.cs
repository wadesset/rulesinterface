﻿using System;

namespace ModelsCore.Interfaces
{
    public interface IBaseModel
    {
        Guid Id { get; set; }
        string Name { get; set; }
        Guid? ParentId { get; set; }
        Guid? GeneralParentId { get; set; }
    }
}
