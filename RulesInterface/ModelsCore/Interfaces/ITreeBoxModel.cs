﻿using System;

namespace ModelsCore.Interfaces
{
    public interface ITreeBoxModel : IBaseBoxModel
    {
        Guid? InnerParentId { get; set; }
    }
}
