﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestRulesInterface.Models;

namespace UnitTestRulesInterface
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void CreateGuid()
        {
            Console.WriteLine(Guid.NewGuid());
            Console.WriteLine(Guid.NewGuid());
            Console.WriteLine(Guid.NewGuid());
            Console.WriteLine(Guid.NewGuid());
            Console.WriteLine(Guid.NewGuid());
            Console.WriteLine(Guid.NewGuid());
            Console.WriteLine(Guid.NewGuid());
            Console.WriteLine(Guid.NewGuid());
            Console.WriteLine(Guid.NewGuid());
        }

        [TestMethod]
        public void TestCreate()
        {
            try
            {
                using (RulesInterfaceContext db = new RulesInterfaceContext())
                {
                    //db.AccessGroupBoxes.Add(new AccessGroupBox() { Id = Guid.NewGuid(), ParentBoxId = default(Guid), SelectedValueId = new[] { Guid.NewGuid() } });
                    //db.SaveChanges();

                    foreach (var group in db.AccessGroupBoxes.ToList())
                        Console.WriteLine(group.SelectedValueIds);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            Console.WriteLine(true);
        }

        [TestMethod]
        public void TestCollection()
        {
            int linqCounter = 0;
            var source = new List<byte> { 0, 0, 1, 0, 1 };

            var bytes = source.Where(x =>
            {
                linqCounter++;
                return x > 0;
            });

            if (bytes.First() == bytes.Last())
            {
                Console.WriteLine(linqCounter--);
            }
            else
            {
                Console.WriteLine(linqCounter++);
            }
        }
    }
}
