﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ModelsCore.Interfaces;
using ModelsCore.Models;
using ModelsCore.ViewModels;
using RulesInterfaceAPICore.Context;

namespace RulesInterfaceAPI.Managers
{
    /// <summary>
    /// Управляет состоянием ящиков 
    /// </summary>
    public class BoxManager
    {
        /// <summary>
        /// Получает ящики с доступами
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        public static async Task<FullTreeView> GetFullTree(string parentId)
        {
            Guid inputParentId = Guid.Parse(parentId);
            FullTreeView view = new FullTreeView();

            try
            {
                view.CompanyGroups = await GetBoxesByParentId<CompanyGroup, CompanyGroupBox>(new[] { inputParentId });
                view.Organizations = await GetBoxesByParentId<Organization, OrganizationBox>(view.CompanyGroups.Select(s => s.Id));
                view.Units = await GetBoxesByParentId<Unit, UnitBox>(view.Organizations.Select(s => s.Id));
                view.Roles = await GetBoxesByParentId<Role, RoleBox>(view.Units.Select(s => s.Id).Concat(view.Organizations.Select(s => s.Id)).Concat(view.CompanyGroups.Select(s => s.Id)));
                view.Statuses = await GetBoxesByParentId<State, StateBox>(view.Units.Select(s => s.Id).Concat(view.Organizations.Select(s => s.Id)).Concat(view.CompanyGroups.Select(s => s.Id)));

                return view;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Получает ящики с доступами
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        public static async Task<List<BoxView>> GetBoxesByParentId<TEntity, TBox>(IEnumerable<Guid> parentIds) where TEntity : class, IBaseModel where TBox : class, IBaseBoxModel
        {
            List<BoxView> list = new List<BoxView>();

            try
            {
                using (RulesInterfaceContext context = new RulesInterfaceContext())
                    foreach (Guid id in parentIds)
                        list.AddRange(await context.Set<TBox>()
                            .Join(context.Set<TEntity>(), p => p.SelectedValueId, c => c.Id, (p, c) => new BoxView() { Id = p.Id, Name = c.Name, SelectedValueId = p.SelectedValueId, ParentBoxId = p.ParentBoxId })
                            .Where(i => i.ParentBoxId == id).ToListAsync());

                return list;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        /// <summary>
        /// Получает ящики с доступами
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        public static async Task<List<BoxView>> GetAccessGroupBoxes<TEntity, TBox>() where TEntity : class, IBaseModel where TBox : class, IBaseBoxModel
        {
            try
            {
                using (RulesInterfaceContext context = new RulesInterfaceContext())
                {
                    List<BoxView> list = new List<BoxView>();
                    list.AddRange(await context.AccessGroupBoxes.Select(p => new BoxView() { Id = p.Id, Name = p.Name == "" || p.Name == null ? "New Group" : p.Name, SelectedValueId = p.SelectedValueId, ParentBoxId = p.ParentBoxId }).ToListAsync());
                    return list;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Создает ящик
        /// </summary>
        public static async Task CreateBox<TBox>() where TBox : class, IBaseBoxModel, new()
        {
            try
            {
                using (RulesInterfaceContext context = new RulesInterfaceContext())
                {
                    Guid newId = Guid.NewGuid();

                    context.Set<TBox>().Add(new TBox() { Id = newId, SelectedValueId = default, ParentBoxId = default });
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Создает ящик
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        public static async Task CreateBox<TBox>(string[] selectedValueId, string parentBoxId) where TBox : class, IBaseBoxModel, new()
        {
            Guid inputParentBoxId = default(Guid);
            List<Guid> inputSelectedValuesId = new List<Guid>();

            if (parentBoxId != null)
                inputParentBoxId = Guid.Parse(parentBoxId);

            if (selectedValueId != null) inputSelectedValuesId.AddRange(selectedValueId.Select(Guid.Parse));

            try
            {
                using (RulesInterfaceContext context = new RulesInterfaceContext())
                {
                    context.Set<TBox>().AddRange(inputSelectedValuesId.Select(s => new TBox { Id = Guid.NewGuid(), SelectedValueId = s, ParentBoxId = inputParentBoxId }));
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Создает ящик
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        public static async Task UpdateBox<TBox>(string selectedValueId, string boxId) where TBox : class, IBaseBoxModel, new()
        {
            try
            {
                Guid inputSelectedValueId = Guid.Parse(selectedValueId);
                Guid inputBoxId = Guid.Parse(boxId);

                using (RulesInterfaceContext context = new RulesInterfaceContext())
                {
                    TBox box = await context.Set<TBox>().FirstOrDefaultAsync(w => w.Id == inputBoxId);
                    box.SelectedValueId = inputSelectedValueId;
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Создает ящик
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        public static async Task UpdateNameBox(string id, string name)
        {
            try
            {
                Guid inputBoxId = Guid.Parse(id);

                using (RulesInterfaceContext context = new RulesInterfaceContext())
                {
                    AccessGroupBox box = await context.AccessGroupBoxes.FirstOrDefaultAsync(w => w.Id == inputBoxId);
                    box.Name = name;
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Удаляет Группу компаний
        /// </summary>
        /// <param name="currentBoxId">Набор идентификаторов (текущий ящик, родительский ящик)</param>
        /// <returns>Коллекция представлений ящика</returns>
        public static async Task DeleteBox<TBox>(string currentBoxId) where TBox : class, IBaseBoxModel
        {
            Guid deletableBoxId = Guid.Parse(currentBoxId);

            try
            {
                using (RulesInterfaceContext context = new RulesInterfaceContext())
                {
                    TBox deletableBox = await context.Set<TBox>().FirstOrDefaultAsync(a => a.Id == deletableBoxId);
                    if (deletableBox != null)
                        context.Set<TBox>().Remove(deletableBox);

                    context.Set<StateBox>().RemoveRange(context.Set<StateBox>().Where(s => s.ParentBoxId == deletableBoxId));

                    await context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        /// <summary>
        /// Получает справочник
        /// </summary>
        /// <returns></returns>
        public static async Task<List<T>> GetEntities<T>() where T : class
        {
            try
            {
                await using RulesInterfaceContext context = new RulesInterfaceContext();
                return await context.Set<T>().ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}