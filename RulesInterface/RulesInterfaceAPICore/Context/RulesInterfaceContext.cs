﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ModelsCore.Models;

namespace RulesInterfaceAPICore.Context
{
    public class RulesInterfaceContext : DbContext
    {
        public virtual DbSet<AccessGroupBox> AccessGroupBoxes { get; set; }
        public virtual DbSet<AccessGroup> AccessGroups { get; set; }
        public virtual DbSet<CompanyGroupBox> CompanyGroupBoxes { get; set; }
        public virtual DbSet<CompanyGroup> CompanyGroups { get; set; }
        public virtual DbSet<OrganizationBox> OrganizationBoxes { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<UnitBox> UnitBoxes { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<RoleBox> RoleBoxes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<StateBox> StatesBoxes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=78.24.217.251;Port=5432;Database=rules_db;Username=wadesset;Password=47113516x");
        }
    }
}