﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModelsCore.Models;
using ModelsCore.ViewModels;
using RulesInterfaceAPI.Managers;

namespace RulesInterfaceAPICore.Controllers
{
    [ApiController]
    public class RulesController : ControllerBase
    {
        /// <summary>
        /// Получает справочник Групп доступа
        /// </summary>
        /// <returns></returns>
        [Route("get-access-group-entities")]
        [HttpGet]
        public async Task<List<AccessGroup>> GetAccessGroupEntities()
        {
            return await BoxManager.GetEntities<AccessGroup>();
        }

        /// <summary>
        /// Получает справочник Групп компаний
        /// </summary>
        /// <returns></returns>
        [Route("get-company-group-entities")]
        [HttpGet]
        public async Task<List<CompanyGroup>> GetCompanyGroupEntities()
        {
            return await BoxManager.GetEntities<CompanyGroup>();
        }

        /// <summary>
        /// Получает справочник Организаций
        /// </summary>
        /// <returns></returns>
        [Route("get-organization-entities")]
        [HttpGet]
        public async Task<List<Organization>> GetOrganizationEntities()
        {
            return await BoxManager.GetEntities<Organization>();
        }

        /// <summary>
        /// Получает справочник Подразделений
        /// </summary>
        /// <returns></returns>
        [Route("get-unit-entities")]
        [HttpGet]
        public async Task<List<Unit>> GetUnitEntities()
        {
            return await BoxManager.GetEntities<Unit>();
        }

        /// <summary>
        /// Получает справочник Должностей
        /// </summary>
        /// <returns></returns>
        [Route("get-role-entities")]
        [HttpGet]
        public async Task<List<Role>> GetRoleEntities()
        {
            return await BoxManager.GetEntities<Role>();
        }

        /// <summary>
        /// Получает справочник Статусов
        /// </summary>
        /// <returns></returns>
        [Route("get-status-entities")]
        [HttpGet]
        public async Task<List<State>> GetStatusEntities()
        {
            return await BoxManager.GetEntities<State>();
        }

        /// <summary>
        /// Получает группы доступа
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("get-access-group-boxes")]
        [HttpGet]
        public async Task<List<BoxView>> GetAccessGroupsSimple()
        {
            return await BoxManager.GetAccessGroupBoxes<AccessGroup, AccessGroupBox>();
        }

        /// <summary>
        /// Получает дерево настроек по выбранной группе доступа
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("get-by-access-group-boxes")]
        [HttpGet]
        public async Task<FullTreeView> GetFullTree(string parentId)
        {
            return await BoxManager.GetFullTree(parentId);
        }

        /// <summary>
        /// Создает группу доступа
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("create-access-group-box")]
        [HttpGet]
        public async Task<List<BoxView>> CreateAccessGroup()
        {
            await BoxManager.CreateBox<AccessGroupBox>();
            return await BoxManager.GetAccessGroupBoxes<AccessGroup, AccessGroupBox>();
        }

        /// <summary>
        /// Создает группу доступа
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("update-access-group-box")]
        [HttpPost]
        public async Task<List<BoxView>> UpdateAccessGroup([FromBody]dynamic ids)
        {
            string selectedValueId = ids.GetProperty("selectedValueId").ToString();
            string boxId = ids.GetProperty("boxId").ToString();

            await BoxManager.UpdateBox<AccessGroupBox>(selectedValueId, boxId);
            return await BoxManager.GetAccessGroupBoxes<AccessGroup, AccessGroupBox>();
        }

        /// <summary>
        /// Создает группу доступа
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("update-name-access-group-box")]
        [HttpPost]
        public async Task<List<BoxView>> UpdateNameAccessGroup([FromBody]dynamic bag)
        {
            string currentId = bag.GetProperty("id").ToString();
            string name = bag.GetProperty("value").ToString();

            await BoxManager.UpdateNameBox(currentId, name);
            return await BoxManager.GetAccessGroupBoxes<AccessGroup, AccessGroupBox>();
        }

        /// <summary>
        /// Создает группу доступа
        /// </summary>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("create-company-group-box")]
        [HttpPost]
        public async Task<FullTreeView> CreateCompanyGroup([FromBody]dynamic ids)
        {
            string[] selectedValueId = ((JsonElement)ids.GetProperty("selectedValueIds")).ToObject<string[]>();
            string parentBoxId = ids.GetProperty("parentBoxId").ToString();

            await BoxManager.CreateBox<CompanyGroupBox>(selectedValueId, parentBoxId);
            return await BoxManager.GetFullTree(parentBoxId);
        }

        /// <summary>
        /// Создает ящик Организаций
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Route("create-organization-box")]
        [HttpPost]
        public async Task<FullTreeView> CreateOrganization([FromBody]dynamic ids)
        {
            string[] selectedValueId = ((JsonElement)ids.GetProperty("selectedValueIds")).ToObject<string[]>();
            string parentBoxId = ids.GetProperty("parentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.CreateBox<OrganizationBox>(selectedValueId, parentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }

        /// <summary>
        /// Создает ящик Подразделений
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Route("create-unit-box")]
        [HttpPost]
        public async Task<FullTreeView> CreateUnit([FromBody]dynamic ids)
        {
            string[] selectedValueId = ((JsonElement)ids.GetProperty("selectedValueIds")).ToObject<string[]>();
            string parentBoxId = ids.GetProperty("parentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.CreateBox<UnitBox>(selectedValueId, parentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }

        /// <summary>
        /// Создает ящик Должностей
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Route("create-role-box")]
        [HttpPost]
        public async Task<FullTreeView> CreateRole([FromBody]dynamic ids)
        {
            string[] selectedValueId = ((JsonElement)ids.GetProperty("selectedValueIds")).ToObject<string[]>();
            string parentBoxId = ids.GetProperty("parentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.CreateBox<RoleBox>(selectedValueId, parentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }

        /// <summary>
        /// Создает ящик Статуса
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Route("create-status-box")]
        [HttpPost]
        public async Task<FullTreeView> CreateStatus([FromBody]dynamic ids)
        {
            string[] selectedValueId = ((JsonElement)ids.GetProperty("selectedValueIds")).ToObject<string[]>();
            string parentBoxId = ids.GetProperty("parentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.CreateBox<StateBox>(selectedValueId, parentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }

        /// <summary>
        /// Удаляет группу доступа
        /// </summary>
        /// <param name="ids">Набор идентификаторов (текущий ящик, родительский ящик(null))</param>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("delete-access-group-box")]
        [HttpPost]
        public async Task<List<BoxView>> DeleteAccessGroupBox([FromBody]dynamic ids)
        {
            string currentBoxId = ids.GetProperty("currentBoxId").ToString();

            await BoxManager.DeleteBox<AccessGroupBox>(currentBoxId);
            return await BoxManager.GetAccessGroupBoxes<AccessGroup, AccessGroupBox>();
        }

        /// <summary>
        /// Удаляет ящик группы компаний
        /// </summary>
        /// <param name="ids">Набор идентификаторов (текущий ящик, родительский ящик)</param>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("delete-company-group-box")]
        [HttpPost]
        public async Task<FullTreeView> DeleteCompanyGroupBox([FromBody]dynamic ids)
        {
            string currentBoxId = ids.GetProperty("currentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.DeleteBox<CompanyGroupBox>(currentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }

        /// <summary>
        /// Удаляет ящик Организации
        /// </summary>
        /// <param name="ids">Набор идентификаторов (текущий ящик, ящик Группы доступа)</param>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("delete-organization-box")]
        [HttpPost]
        public async Task<FullTreeView> DeleteOrganizationBox([FromBody]dynamic ids)
        {
            string currentBoxId = ids.GetProperty("currentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.DeleteBox<OrganizationBox>(currentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }

        /// <summary>
        /// Удаляет ящик Подразделения
        /// </summary>
        /// <param name="ids">Набор идентификаторов (текущий ящик, ящик Группы доступа)</param>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("delete-unit-box")]
        [HttpPost]
        public async Task<FullTreeView> DeleteUnitBox([FromBody]dynamic ids)
        {
            string currentBoxId = ids.GetProperty("currentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.DeleteBox<UnitBox>(currentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }

        /// <summary>
        /// Удаляет ящик Должностей
        /// </summary>
        /// <param name="ids">Набор идентификаторов (текущий ящик, ящик Группы доступа)</param>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("delete-role-box")]
        [HttpPost]
        public async Task<FullTreeView> DeleteRoleBox([FromBody]dynamic ids)
        {
            string currentBoxId = ids.GetProperty("currentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.DeleteBox<RoleBox>(currentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }

        /// <summary>
        /// Удаляет ящик статусов
        /// </summary>
        /// <param name="ids">Набор идентификаторов (текущий ящик, Группы доступа)</param>
        /// <returns>Коллекция представлений ящика</returns>
        [Route("delete-status-box")]
        [HttpPost]
        public async Task<FullTreeView> DeleteStatusBox([FromBody]dynamic ids)
        {
            string currentBoxId = ids.GetProperty("currentBoxId").ToString();
            string accessGroupId = ids.GetProperty("accessGroupId").ToString();

            await BoxManager.DeleteBox<StateBox>(currentBoxId);
            return await BoxManager.GetFullTree(accessGroupId);
        }
    }
    public static partial class JsonExtensions
    {
        public static T ToObject<T>(this JsonElement element, JsonSerializerOptions options = null)
        {
            var bufferWriter = new ArrayBufferWriter<byte>();
            using (var writer = new Utf8JsonWriter(bufferWriter))
                element.WriteTo(writer);
            return JsonSerializer.Deserialize<T>(bufferWriter.WrittenSpan, options);
        }

        public static T ToObject<T>(this JsonDocument document, JsonSerializerOptions options = null)
        {
            if (document == null)
                throw new ArgumentNullException(nameof(document));
            return document.RootElement.ToObject<T>(options);
        }
    }
}