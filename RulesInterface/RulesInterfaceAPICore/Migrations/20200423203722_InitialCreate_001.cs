﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RulesInterfaceAPICore.Migrations
{
    public partial class InitialCreate_001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccessGroupBoxes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ParentBoxId = table.Column<Guid>(nullable: false),
                    SelectedValueId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessGroupBoxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccessGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    GeneralParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompanyGroupBoxes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ParentBoxId = table.Column<Guid>(nullable: false),
                    SelectedValueId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyGroupBoxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompanyGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    GeneralParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrganizationBoxes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ParentBoxId = table.Column<Guid>(nullable: false),
                    SelectedValueId = table.Column<Guid>(nullable: false),
                    SelectedValueRelationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationBoxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Organizations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    GeneralParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoleBoxes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ParentBoxId = table.Column<Guid>(nullable: false),
                    SelectedValueId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleBoxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    GeneralParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "States",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    GeneralParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_States", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StatesBoxes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ParentBoxId = table.Column<Guid>(nullable: false),
                    SelectedValueId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatesBoxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UnitBoxes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ParentBoxId = table.Column<Guid>(nullable: false),
                    SelectedValueId = table.Column<Guid>(nullable: false),
                    InnerParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitBoxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    GeneralParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccessGroupBoxes");

            migrationBuilder.DropTable(
                name: "AccessGroups");

            migrationBuilder.DropTable(
                name: "CompanyGroupBoxes");

            migrationBuilder.DropTable(
                name: "CompanyGroups");

            migrationBuilder.DropTable(
                name: "OrganizationBoxes");

            migrationBuilder.DropTable(
                name: "Organizations");

            migrationBuilder.DropTable(
                name: "RoleBoxes");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "States");

            migrationBuilder.DropTable(
                name: "StatesBoxes");

            migrationBuilder.DropTable(
                name: "UnitBoxes");

            migrationBuilder.DropTable(
                name: "Units");
        }
    }
}
