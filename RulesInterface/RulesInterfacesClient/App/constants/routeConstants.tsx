﻿export const routeConstants =
{
    PATH_URL: 'https://localhost:44345',

    GET_BY_ACCESS_GROUP_BOXES_URL: '/get-by-access-group-boxes',

    GET_ACCESS_GROUP_BOXES_URL: '/get-access-group-boxes',
    CREATE_ACCESS_GROUP_BOX_URL: '/create-access-group-box',
    UPDATE_ACCESS_GROUP_BOX_URL: '/update-access-group-box',
    UPDATE_NAME_ACCESS_GROUP_BOX_URL: '/update-name-access-group-box',
    DELETE_ACCESS_GROUP_BOX_URL: '/delete-access-group-box',

    GET_COMPANY_GROUPS_URL: '/get-company-group-boxes',
    CREATE_COMPANY_GROUPS_URL: '/create-company-group-box',
    DELETE_COMPANY_GROUP_BOX_URL: '/delete-company-group-box',

    GET_ORGANIZATION_BOXES_URL: '/get-organization-boxes',
    CREATE_ORGANIZATION_BOX_URL: '/create-organization-box',
    UPDATE_ORGANIZATION_BOX_URL: '/update-organization-box',
    DELETE_ORGANIZATION_BOX_URL: '/delete-organization-box',

    CREATE_UNIT_BOX_URL: '/create-unit-box',
    DELETE_UNIT_BOX_URL: '/delete-unit-box',

    GET_ROLE_BOXES_URL: '/get-role-boxes',
    CREATE_ROLE_BOX_URL: '/create-role-box',
    DELETE_ROLE_BOX_URL: '/delete-role-box',

    GET_STATUS_BOXES_URL: '/get-status-boxes',
    CREATE_STATUS_BOX_URL: '/create-status-box',
    UPDATE_STATUS_BOX_URL: '/update-status-box',
    DELETE_STATUS_BOX_URL: '/delete-status-box',

    GET_ACCESS_GROUP_ENTITY: '/get-access-group-entities',
    GET_COMPANY_GROUP_ENTITY: '/get-company-group-entities',
    GET_ORGANIZATION_ENTITY: '/get-organization-entities',
    GET_UNIT_ENTITY: '/get-unit-entities',
    GET_ROLE_ENTITY: '/get-role-entities',
    GET_STATUS_ENTITY: '/get-status-entities',
}