import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Main from './main/main';

export default class App extends React.Component {
    render() {
        return (
            <Router>
                <div id="document">
                    <main>
                        <Switch>
                            <Route path="/main" component={Main} />
                            <Route exact path="/" render={() => (<Redirect to="/main" />)} />
                        </Switch>
                    </main>
                </div>
            </Router>
        );
    }
}