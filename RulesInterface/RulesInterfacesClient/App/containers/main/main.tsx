import * as React from 'react';
import 'react-dropdown/style.css'
import { connect } from 'react-redux';
import Settings from "../../components/Settings";
import AccessGroupSettings from "../../components/AccessGroupSettings";
import LeftMenu from "../../components/LeftMenu";

interface IProps {
    openedSettings: boolean;
}

class Main extends React.Component<IProps, {}> {
    render() {
        let settings = this.props.openedSettings ? <AccessGroupSettings/> : <Settings/>;

        return (
            <div className="container-fluid">
                <div className="mainSettingsSquareLabel appLabel">Nakovalni optom</div>
                <hr/>
                <div className="row">
                    <div className="col-sm-2">
                        <LeftMenu />
                    </div>

                    <div className="col-sm-9">
                        {settings}
                    </div>
                </div>
            </div>
        );
    }
}

let mapProps = (state) => {
    return {
        openedSettings: state.editorReducer.openedSettings
    };
};

let mapDispatch = (dispatch) => {
    return {
    };
};

export default connect(mapProps, mapDispatch)(Main);