export interface IModelProps {
    id: string;
    name: string;
    parentId: string;
    generalParentId: string;
}

export interface IBox {
    id: string;
    models: IModelProps[];
    selectedValues: string[];
}

export interface IFullTreeProps {
    id: string;
    companyGroups: IBoxProps[];
    organizations: IBoxProps[];
    units: IBoxProps[];
    roles: IBoxProps[];
    statuses: IBoxProps[];
}

export interface IBoxProps {
    id: string;
    name: string;
    parentBoxId: string;
    selectedValueId: string;
}

export interface ITreeData {
    label: string;
    value: string;
    children: ITreeData[];
}