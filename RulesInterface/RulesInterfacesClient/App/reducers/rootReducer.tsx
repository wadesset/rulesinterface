import { combineReducers } from 'redux';
import mainReducer from "./mainReducer";
import editorReducer from "./editorReducer";

export default combineReducers({
    mainReducer,
    editorReducer
});