export const utilsConstants = {
    DEFAULT_GUID: '00000000-0000-0000-0000-000000000000',
    COMPANY_GROUP_SYS_CODE: 'CompanyGroup',
    ORGANIZATION_SYS_CODE: 'Organization',
    UNIT_SYS_CODE: 'Unit',
    ROLE_SYS_CODE: 'Role',
};
