import * as React from 'react';
import SettingsItemBox from "./standart/SettingsItemBox";
export default class SettingSquare extends React.Component {
    constructor(props) {
        super(props);
        this.state = { switchEditor: false, switchAdditionalEditor: false, checkedId: null };
    }
    //Удаляет дубликаты// так себе конечно...
    removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    }
    onSelect(selectedItemId, alreadyChecked) {
        if (alreadyChecked) {
            this.props.onSelect(null);
            this.setState({ checkedId: null });
        }
        else {
            this.props.onSelect(selectedItemId);
            this.setState({ checkedId: selectedItemId });
        }
    }
    onDelete(currentBoxId) {
        this.props.onDelete(currentBoxId);
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.source === nextProps.source && nextState === this.state.switchEditor) {
            return false;
        }
        return true;
    }
    switchEditor() {
        if (this.state.switchAdditionalEditor) {
            this.setState({ switchAdditionalEditor: false }, () => { this.setState({ switchEditor: !this.state.switchEditor }); });
        }
        else {
            this.setState({ switchEditor: !this.state.switchEditor });
        }
    }
    switchAdditionalEditor() {
        if (this.state.switchEditor) {
            this.setState({ switchEditor: false }, () => { this.setState({ switchAdditionalEditor: !this.state.switchAdditionalEditor }); });
        }
        else {
            this.setState({ switchAdditionalEditor: !this.state.switchAdditionalEditor });
        }
    }
    render() {
        let editor = React.createElement("div", { className: "settings-square-editor" }, this.props.editor);
        let additionalEditor = React.createElement("div", { className: "settings-square-editor" }, this.props.additionalEditor);
        let sourceList = this.props.source.map(item => {
            let background = this.props.source.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            let checked = this.state.checkedId === item.id;
            return (React.createElement("div", { key: item.id, className: background },
                React.createElement(SettingsItemBox, { checked: checked, groupName: this.props.groupName, label: item.name, parentId: item.parentBoxId, structureBtn: this.props.itemStructureBtn, select: () => { this.onSelect.bind(this)(item.id, checked); }, delete: this.onDelete.bind(this), value: item.id, status: true })));
        });
        let viewer = React.createElement("div", { className: "settings-square" }, sourceList);
        let switchEditor = this.state.switchEditor ? editor : viewer;
        let additionalSwitchEditor = this.state.switchAdditionalEditor ? additionalEditor : switchEditor;
        let additionalBtn = this.props.additionalBtn
            ? React.createElement("div", { className: "col-sm ta-r" },
                React.createElement("input", { className: "addSettingsItem", disabled: this.props.disabled, type: "button", onClick: this.switchAdditionalEditor.bind(this), value: this.props.additionalBtnLabel }))
            : null;
        return (React.createElement("div", { className: "wd-100 p-r-30" },
            React.createElement("div", { className: "row mb-3" },
                React.createElement("div", { className: "col-sm settingsSquareLabel" }, this.props.squareLabel),
                additionalBtn,
                React.createElement("div", { className: "col-sm ta-r" },
                    React.createElement("input", { className: "addSettingsItem", disabled: this.props.disabled, type: "button", onClick: this.switchEditor.bind(this), value: this.props.btnLabel }))),
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm" }, additionalSwitchEditor))));
    }
}
SettingSquare.defaultProps = {
    additionalBtnLabel: "",
    itemStructureBtn: false,
};
