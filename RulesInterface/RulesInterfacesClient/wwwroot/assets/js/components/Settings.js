var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as React from "react";
import SettingSquare from "./SettingSquare";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { selectParent } from "../actions/mainActions";
import { createBox, deleteBox } from "../actions/editorActions";
import { actionConstants } from "../constants/actionConstants";
import { utilsConstants } from "../constants/utilsConstants";
import { routeConstants } from "../constants/routeConstants";
import CustomListView from "./multiselect/CustomListView";
import CustomTwoColumnsListView from "./multiselect/CustomTwoColumnsListView";
import CustomTwoColumnsTreeListView from "./multiselect/CustomTwoColumnsTreeListView";
class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.companyGroupRef = React.createRef();
        this.organizationRef = React.createRef();
        this.unitRef = React.createRef();
        this.roleRef = React.createRef();
    }
    onSelectCompanyGroup(selectedItemId) {
        this.props.selectParent(selectedItemId, actionConstants.SELECT_COMPANY_GROUP_VALUE);
    }
    onSelectOrganization(selectedItemId) {
        this.props.selectParent(selectedItemId, actionConstants.SELECT_ORGANIZATION_VALUE);
    }
    onSelectUnit(selectedItemId) {
        this.props.selectParent(selectedItemId, actionConstants.SELECT_UNITS_VALUE);
    }
    onSelectRole(selectedItemId) {
        this.props.selectParent(selectedItemId, actionConstants.SELECT_ROLES_VALUE);
    }
    createCompanyGroup(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.createBox(ids, this.props.parentId, null, routeConstants.CREATE_COMPANY_GROUPS_URL, actionConstants.CREATE_COMPANY_GROUPS_URL);
            this.companyGroupRef.current.switchEditor();
        });
    }
    createOrganization(ids, parentId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.createBox(ids, parentId, this.props.parentId, routeConstants.CREATE_ORGANIZATION_BOX_URL, actionConstants.CREATE_ORGANIZATION_BOX_URL);
            this.organizationRef.current.switchEditor();
        });
    }
    createUnit(ids, parentId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.createBox(ids, parentId, this.props.parentId, routeConstants.CREATE_UNIT_BOX_URL, actionConstants.CREATE_UNIT_BOX_URL);
            this.unitRef.current.switchEditor();
        });
    }
    createRole(ids, parentId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.createBox(ids, parentId, this.props.parentId, routeConstants.CREATE_ROLE_BOX_URL, actionConstants.CREATE_ROLE_BOX_URL);
            this.roleRef.current.switchEditor();
        });
    }
    createStatus(ids, parentId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.createBox(ids, parentId, this.props.parentId, routeConstants.CREATE_STATUS_BOX_URL, actionConstants.CREATE_STATUS_BOX);
            this.roleRef.current.switchAdditionalEditor();
        });
    }
    deleteCompanyGroup(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.deleteBox(ids, this.props.parentId, routeConstants.DELETE_COMPANY_GROUP_BOX_URL, actionConstants.DELETE_COMPANY_GROUPS_URL);
        });
    }
    deleteOrganization(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.deleteBox(ids, this.props.parentId, routeConstants.DELETE_ORGANIZATION_BOX_URL, actionConstants.DELETE_ORGANIZATION_BOX_URL);
        });
    }
    deleteUnit(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.deleteBox(ids, this.props.parentId, routeConstants.DELETE_UNIT_BOX_URL, actionConstants.DELETE_UNIT_BOX_URL);
        });
    }
    deleteRole(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.deleteBox(ids, this.props.parentId, routeConstants.DELETE_ROLE_BOX_URL, actionConstants.DELETE_ROLE_BOX_URL);
        });
    }
    render() {
        const companyGroupLabel = [{ id: utilsConstants.COMPANY_GROUP_SYS_CODE, name: "SubGroups", parentBoxId: null, selectedValueId: null }];
        const organizationLabel = [{ id: utilsConstants.UNIT_SYS_CODE, name: "Stacks", parentBoxId: null, selectedValueId: null }];
        const unitLabel = [{ id: utilsConstants.ROLE_SYS_CODE, name: "Units", parentBoxId: null, selectedValueId: null }];
        const fullBoxesList = companyGroupLabel.concat(this.props.companyGroups).concat(organizationLabel).concat(this.props.organizations).concat(unitLabel).concat(this.props.units);
        const companyGroupsEditor = React.createElement(CustomListView, { source: this.props.companyGroupEntities, apply: this.createCompanyGroup.bind(this) });
        const organizationsEditor = React.createElement(CustomTwoColumnsListView, { source: this.props.organizationEntities, parentSource: this.props.companyGroups, visibleSelectAllBtn: false, visibleUnselectAllBtn: false, apply: this.createOrganization.bind(this) });
        const unitsEditor = React.createElement(CustomTwoColumnsTreeListView, { source: this.props.unitEntities, parentSource: this.props.organizations, apply: this.createUnit.bind(this) });
        const rolesEditor = React.createElement(CustomTwoColumnsListView, { source: this.props.roleEntities, parentSource: fullBoxesList, visibleSelectAllBtn: false, visibleUnselectAllBtn: false, apply: this.createRole.bind(this) });
        const statusEditor = React.createElement(CustomTwoColumnsListView, { source: this.props.statusEntities, parentSource: fullBoxesList, visibleSelectAllBtn: false, visibleUnselectAllBtn: false, apply: this.createStatus.bind(this) });
        return (React.createElement("div", { className: "mainSettingsBox" },
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm mainSettingsSquareLabel" }, "Main settings")),
            React.createElement("div", { className: "row mb-60" },
                React.createElement("div", { className: "col-sm" },
                    React.createElement(SettingSquare, { ref: this.companyGroupRef, disabled: this.props.allowedToManage, source: this.props.companyGroups, groupName: "CompanyGroups", editor: companyGroupsEditor, onSelect: this.onSelectCompanyGroup.bind(this), onDelete: this.deleteCompanyGroup.bind(this), squareLabel: "SubGroups", btnLabel: "+Add new SubGroup" })),
                React.createElement("div", { className: "col-sm" },
                    React.createElement(SettingSquare, { ref: this.organizationRef, disabled: this.props.allowedToManage, source: this.props.organizations, groupName: "Organizations", editor: organizationsEditor, itemStructureBtn: true, onSelect: this.onSelectOrganization.bind(this), onDelete: this.deleteOrganization.bind(this), squareLabel: "Stacks", btnLabel: "+Add new Stack" }))),
            React.createElement("div", { className: "row mb-60" },
                React.createElement("div", { className: "col-sm" },
                    React.createElement(SettingSquare, { ref: this.unitRef, disabled: this.props.allowedToManage, source: this.props.units, groupName: "Units", editor: unitsEditor, itemStructureBtn: true, onSelect: this.onSelectUnit.bind(this), onDelete: this.deleteUnit.bind(this), squareLabel: "Units", btnLabel: "+Add new Unit" })),
                React.createElement("div", { className: "col-sm" },
                    React.createElement(SettingSquare, { ref: this.roleRef, disabled: this.props.allowedToManage, source: this.props.roles, groupName: "Roles", editor: rolesEditor, itemStructureBtn: true, additionalBtn: true, additionalBtnLabel: "+Add new Status", additionalEditor: statusEditor, onSelect: this.onSelectRole.bind(this), onDelete: this.deleteRole.bind(this), squareLabel: "Roles/Statuses", btnLabel: "+Add new Role" })))));
    }
}
let mapProps = (state) => {
    return {
        fullTree: state.mainReducer.fullTree,
        companyGroups: state.mainReducer.companyGroups,
        organizations: state.mainReducer.organizations,
        units: state.mainReducer.units,
        roles: state.mainReducer.roles,
        companyGroupEntities: state.editorReducer.companyGroupEntities,
        organizationEntities: state.editorReducer.organizationEntities,
        unitEntities: state.editorReducer.unitEntities,
        roleEntities: state.editorReducer.roleEntities,
        statusEntities: state.editorReducer.statusEntities,
        allowedToManage: state.editorReducer.allowedToManage,
        parentId: state.mainReducer.parentId,
        error: state.mainReducer.error
    };
};
let mapDispatch = (dispatch) => {
    return {
        selectParent: bindActionCreators(selectParent, dispatch),
        createBox: bindActionCreators(createBox, dispatch),
        deleteBox: bindActionCreators(deleteBox, dispatch)
    };
};
export default connect(mapProps, mapDispatch)(Settings);
