var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as React from 'react';
import { getFullTree, getAccessGroups, setDefaultStates, setParentItem, createAccessGroup } from "../actions/mainActions";
import BoxSimple from "../components/standart/BoxSimple";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getAccessGroupEntities, getCompanyGroupEntities, getOrganizationEntities, getUnitEntities, getRoleEntities, getStatusEntities, allowedToManage, openSettings } from "../actions/editorActions";
class LeftMenu extends React.Component {
    constructor(props) {
        super(props);
        this.props.getAccessGroups();
        this.props.getAccessGroupEntities();
        this.props.getCompanyGroupEntities();
        this.props.getOrganizationEntities();
        this.props.getUnitEntities();
        this.props.getRoleEntities();
        this.props.getStatusEntities();
    }
    getFullTree(parentId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.props.getFullTree(parentId);
            yield this.props.setDefaultStates();
            yield this.props.allowedToManage(false);
            yield this.props.setParentItem(parentId);
        });
    }
    createAccessGroup() {
        this.props.createAccessGroup();
    }
    openSettings(value) {
        this.props.openSettings(true);
    }
    render() {
        let accessList = this.props.accessGroups ? this.props.accessGroups.map(item => {
            return (React.createElement("div", { key: item.id, className: "menu-item" },
                React.createElement("input", { className: "display-n", type: "radio", name: "menu", id: item.id }),
                React.createElement("label", { className: "w-100", key: item.id, htmlFor: item.id, onClick: () => this.getFullTree.bind(this)(item.id) },
                    React.createElement(BoxSimple, { label: item.name, value: item.id, openSettings: this.openSettings.bind(this) }))));
        }) : [];
        return (React.createElement("div", { className: "leftScrollMenuBox min-wd-235" },
            React.createElement("div", { className: "redThing" }),
            React.createElement("div", { className: "row leftScrollMenuSubBox" },
                React.createElement("div", { className: "col-sm" },
                    React.createElement("div", { className: "row" },
                        React.createElement("div", { className: "col-sm mainSettingsSquareLabel" }, "Groups")),
                    React.createElement("div", { className: "row" },
                        React.createElement("div", { className: "col-sm" },
                            React.createElement("input", { className: "addAccessBtn", type: "button", value: "+ Add new group", onClick: this.createAccessGroup.bind(this) }))),
                    React.createElement("div", { className: "row" },
                        React.createElement("div", { className: "col-sm" }, accessList))))));
    }
}
let mapProps = (state) => {
    return {
        accessGroups: state.mainReducer.accessGroups,
        fullTree: state.mainReducer.fullTree,
        error: state.mainReducer.error
    };
};
let mapDispatch = (dispatch) => {
    return {
        getAccessGroups: bindActionCreators(getAccessGroups, dispatch),
        getFullTree: bindActionCreators(getFullTree, dispatch),
        setDefaultStates: bindActionCreators(setDefaultStates, dispatch),
        getAccessGroupEntities: bindActionCreators(getAccessGroupEntities, dispatch),
        getCompanyGroupEntities: bindActionCreators(getCompanyGroupEntities, dispatch),
        getOrganizationEntities: bindActionCreators(getOrganizationEntities, dispatch),
        getUnitEntities: bindActionCreators(getUnitEntities, dispatch),
        getRoleEntities: bindActionCreators(getRoleEntities, dispatch),
        getStatusEntities: bindActionCreators(getStatusEntities, dispatch),
        allowedToManage: bindActionCreators(allowedToManage, dispatch),
        openSettings: bindActionCreators(openSettings, dispatch),
        setParentItem: bindActionCreators(setParentItem, dispatch),
        createAccessGroup: bindActionCreators(createAccessGroup, dispatch)
    };
};
export default connect(mapProps, mapDispatch)(LeftMenu);
