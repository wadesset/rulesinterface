import * as React from 'react';
import { connect } from "react-redux";
class SettingsItemBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = { openStatus: false };
    }
    select() {
        this.props.select(this.props.value, this.props.checked);
    }
    delete() {
        this.props.delete(this.props.value);
    }
    render() {
        let statusLabel = [];
        if (this.props.value) {
            statusLabel = this.props.statuses.filter(f => f.parentBoxId === this.props.value) ? this.props.statuses.filter(f => f.parentBoxId === this.props.value).map(m => {
                return (React.createElement("div", { key: m.name, className: "statusInfoItem" }, m.name));
            }) : [];
        }
        let parentsource = this.props.fullTree.companyGroups.concat(this.props.fullTree.organizations).concat(this.props.fullTree.units);
        let parentBoxFirstLayer = parentsource.filter(f => f.id === this.props.parentId)[0];
        let parentBoxSecondLayer;
        if (parentBoxFirstLayer) {
            parentBoxSecondLayer = parentsource.filter(f => f.id === parentBoxFirstLayer.parentBoxId)[0];
        }
        let parentBoxThirdLayer;
        if (parentBoxSecondLayer) {
            parentBoxThirdLayer = parentsource.filter(f => f.id === parentBoxSecondLayer.ParentBoxId)[0];
        }
        let secondLayer;
        if (parentBoxSecondLayer) {
            secondLayer = React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm-1" },
                    React.createElement("i", { className: "structure-info-ic" })),
                React.createElement("div", { className: "col-sm" }, parentBoxSecondLayer ? parentBoxSecondLayer.Name : null));
        }
        let thirdLayer;
        if (parentBoxThirdLayer) {
            thirdLayer = React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm-1" },
                    React.createElement("i", { className: "structure-point-ic" })),
                React.createElement("div", { className: "col-sm" }, parentBoxThirdLayer ? parentBoxThirdLayer.Name : null));
        }
        //Если необходимо добавить доп. (i) кнопку - раскомментировать.
        //let infoBtn = <div className="helpInformationBtn">i</div>;
        let statusInfo = React.createElement("div", { className: "statusInfo" }, statusLabel);
        let structureInfo = React.createElement("div", { className: "structureInfo" },
            React.createElement("div", { className: "container-fluid" },
                React.createElement("div", { className: "row" },
                    React.createElement("div", { className: "col-sm p-3 structureInfoLabel" }, "\u0421\u0442\u0440\u0443\u043A\u0442\u0443\u0440\u0430 \u043A\u043E\u043C\u043F\u0430\u043D\u0438\u0438")),
                React.createElement("div", { className: "row" },
                    React.createElement("div", { className: "col-sm-1 mt-3" },
                        React.createElement("i", { className: "structure-info-ic" })),
                    React.createElement("div", { className: "col-sm p-3" }, parentBoxFirstLayer ? parentBoxFirstLayer.name : null)),
                secondLayer,
                thirdLayer));
        let status = React.createElement("div", { className: "statusBtn" },
            statusInfo,
            `Статусы (${statusLabel.length})`);
        let deleteBtn = this.props.checked ? React.createElement("div", { className: "deleteBtnItem", onClick: this.delete.bind(this) },
            React.createElement("i", { className: "item-delete-ic" })) : null;
        let structureBtn = this.props.structureBtn ? React.createElement("div", { className: "structureBtn" },
            structureInfo,
            React.createElement("i", { className: "structure-ic" })) : null;
        let color = this.props.checked ? "redLabel" : null;
        return (React.createElement("div", { className: "accessItem", onClick: this.select.bind(this) },
            React.createElement("span", { className: color }, this.props.label),
            deleteBtn,
            status,
            structureBtn));
    }
}
let mapProps = (state) => {
    return {
        statuses: state.mainReducer.statuses,
        fullTree: state.mainReducer.fullTree
    };
};
let mapDispatch = (dispatch) => {
    return {};
};
export default connect(mapProps, mapDispatch)(SettingsItemBox);
