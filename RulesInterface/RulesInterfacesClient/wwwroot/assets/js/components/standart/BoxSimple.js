import * as React from 'react';
export default class BoxSimple extends React.Component {
    openSettings() {
        this.props.openSettings(this.props.value);
    }
    render() {
        return (React.createElement("div", { className: "accessItem" },
            React.createElement("i", { className: "menu-item-icon dot-ic" }),
            React.createElement("span", null, this.props.label),
            React.createElement("div", { className: "settingsBtn", onClick: this.openSettings.bind(this) },
                React.createElement("i", { className: "settings-wheel-ic" }))));
    }
}
