import * as React from 'react';
import CheckItem from "./CheckItem";
export default class CustomTwoColumnsListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = { list: this.props.source, checkedList: this.props.selectedItems, checkedParentList: [], parentList: this.props.parentSource };
    }
    apply() {
        this.props.apply(this.state.checkedList, this.state.checkedParentList[0]);
    }
    onChange(event) {
        this.setState({ list: this.props.source.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }
    onChangeParent(event) {
        this.setState({ parentList: this.props.parentSource.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }
    checkAll() {
        this.setState({ checkedList: this.state.list.map(m => m.id) });
    }
    uncheckAll() {
        this.setState({ checkedList: [] });
    }
    addToCheckedList(id) {
        this.setState({ checkedList: this.state.checkedList.concat(id) });
    }
    removeFromCheckedList(id) {
        let list = this.state.checkedList;
        const index = list.indexOf(id);
        list.splice(index, 1);
        this.setState({ checkedList: list });
    }
    addToCheckedParentList(id) {
        this.setState({ checkedParentList: this.state.checkedParentList.concat(id) });
    }
    removeFromCheckedParentList(id) {
        let list = this.state.checkedParentList;
        const index = list.indexOf(id);
        list.splice(index, 1);
        this.setState({ checkedParentList: list });
    }
    render() {
        let list = this.state.list ? this.state.list.map(item => {
            let checked = this.state.checkedList.includes(item.id);
            let background = this.props.source.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            return (React.createElement(CheckItem, { key: item.id, id: item.id, name: item.name, checked: checked, background: background, addToCheckedList: this.addToCheckedList.bind(this), removeFromCheckedList: this.removeFromCheckedList.bind(this) }));
        }) : [];
        let parentList = this.state.parentList.map(item => {
            let checked = this.state.checkedParentList.includes(item.id);
            let background = this.props.parentSource.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            return (React.createElement(CheckItem, { key: item.id, id: item.id, name: item.name, checked: checked, background: background, addToCheckedList: this.addToCheckedParentList.bind(this), removeFromCheckedList: this.removeFromCheckedParentList.bind(this) }));
        });
        let btnApplyAll = this.props.visibleSelectAllBtn ? React.createElement("input", { className: "btnApplyAll", type: "button", onClick: this.checkAll.bind(this), value: "\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0432\u0441\u0435" }) : null;
        let btnCancelAll = this.props.visibleUnselectAllBtn ? React.createElement("input", { className: "btnCancelAll", type: "button", onClick: this.uncheckAll.bind(this), value: "\u0421\u043D\u044F\u0442\u044C \u0432\u044B\u0434\u0435\u043B\u0435\u043D\u0438\u0435" }) : null;
        return (React.createElement("div", { className: "container" },
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm" },
                    React.createElement("input", { className: "editSearch", placeholder: "\u041F\u043E\u0438\u0441\u043A", onChange: this.onChange.bind(this) }),
                    React.createElement("div", { className: "col-sm editSourceList mt-2" }, list)),
                React.createElement("div", { className: "col-sm" },
                    React.createElement("input", { className: "editSearch", placeholder: "\u041F\u043E\u0438\u0441\u043A", onChange: this.onChangeParent.bind(this) }),
                    React.createElement("div", { className: "col-sm editSourceList mt-2" }, parentList))),
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm editListViewNavigation" },
                    React.createElement("input", { className: "btnApply", type: "button", value: "\u0413\u043E\u0442\u043E\u0432\u043E", onClick: this.apply.bind(this) }),
                    btnApplyAll,
                    btnCancelAll))));
    }
}
CustomTwoColumnsListView.defaultProps = {
    selectedItems: [],
    visibleSelectAllBtn: true,
    visibleUnselectAllBtn: true
};
