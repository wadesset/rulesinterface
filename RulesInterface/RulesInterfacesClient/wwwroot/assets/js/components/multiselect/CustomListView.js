import * as React from 'react';
import CheckItem from "./CheckItem";
export default class CustomListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = { list: this.props.source, checkedList: this.props.selectedItems };
    }
    apply() {
        this.props.apply(this.state.checkedList);
    }
    onChange(event) {
        this.setState({ list: this.props.source.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }
    checkAll() {
        this.setState({ checkedList: this.state.list.map(m => m.id) });
    }
    uncheckAll() {
        this.setState({ checkedList: [] });
    }
    addToCheckedList(id) {
        this.setState({ checkedList: this.state.checkedList.concat(id) });
    }
    removeFromCheckedList(id) {
        let list = this.state.checkedList;
        const index = list.indexOf(id);
        list.splice(index, 1);
        this.setState({ checkedList: list });
    }
    render() {
        let list = this.state.list ? this.state.list.map(item => {
            let checked = this.state.checkedList.includes(item.id);
            let background = this.props.source.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            return (React.createElement(CheckItem, { key: item.id, id: item.id, name: item.name, checked: checked, background: background, addToCheckedList: this.addToCheckedList.bind(this), removeFromCheckedList: this.removeFromCheckedList.bind(this) }));
        }) : [];
        return (React.createElement("div", { className: "container" },
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm" },
                    React.createElement("input", { className: "editSearch", placeholder: "Search", onChange: this.onChange.bind(this) }),
                    React.createElement("div", { className: "col-sm editSourceList mt-2" }, list))),
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm editListViewNavigation" },
                    React.createElement("input", { className: "btnApply", type: "button", value: "Apply", onClick: this.apply.bind(this) }),
                    React.createElement("input", { className: "btnApplyAll", type: "button", onClick: this.checkAll.bind(this), value: "All" }),
                    React.createElement("input", { className: "btnCancelAll", type: "button", onClick: this.uncheckAll.bind(this), value: "Take off all" })))));
    }
}
CustomListView.defaultProps = {
    selectedItems: []
};
