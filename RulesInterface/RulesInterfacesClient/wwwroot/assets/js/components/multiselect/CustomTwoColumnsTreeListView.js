import * as React from 'react';
import CheckItem from "./CheckItem";
import CheckboxTree from 'react-checkbox-tree';
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
export default class CustomTwoColumnsTreeListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            checkedParentList: [],
            parentList: this.props.parentSource,
            checked: [],
            expanded: []
        };
    }
    apply() {
        this.props.apply(this.state.checked, this.state.checkedParentList[0]);
    }
    onChange(event) {
        this.setState({ list: this.props.source.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }
    onChangeParent(event) {
        this.setState({ parentList: this.props.parentSource.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }
    addToCheckedParentList(id) {
        this.setState({ checkedParentList: this.state.checkedParentList.concat(id) });
        this.setState({ list: this.props.source.filter(f => f.generalParentId === this.props.parentSource.filter(f => f.id === id)[0].selectedValueId) });
    }
    removeFromCheckedParentList(id) {
        let list = this.state.checkedParentList;
        const index = list.indexOf(id);
        list.splice(index, 1);
        this.setState({ checkedParentList: list });
        this.setState({ list: [] });
    }
    onChangeTree(checked, targetNode) {
        if (!targetNode.checked) {
            this.setState({ checked: this.state.checked.concat(targetNode.value) });
        }
        else {
            let list = this.state.checked;
            const index = list.indexOf(targetNode.value);
            list.splice(index, 1);
            this.setState({ checked: list });
        }
    }
    render() {
        const treeSource = this.state.list ? this.state.list.filter(f => f.parentId === null).map(item => {
            let j = this.state.list.filter((f) => { return f.parentId === item.id; });
            if (j !== undefined && j.length > 0) {
                return {
                    label: item.name,
                    value: item.id,
                    className: "treeNodeText ",
                    children: this.state.list.filter((f) => { return f.parentId === item.id; }).map(child => {
                        return {
                            label: child.name,
                            value: child.id,
                            className: "treeNodeText ",
                        };
                    })
                };
            }
            else {
                let background = this.state.list.filter(f => f.parentId === null).indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
                return {
                    label: item.name,
                    value: item.id,
                    className: "treeNodeText " + background,
                };
            }
        }) : [];
        let parentList = this.state.parentList.map(item => {
            let checked = this.state.checkedParentList.includes(item.id);
            let background = this.props.parentSource.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            return (React.createElement(CheckItem, { key: item.id, id: item.id, name: item.name, checked: checked, background: background, addToCheckedList: this.addToCheckedParentList.bind(this), removeFromCheckedList: this.removeFromCheckedParentList.bind(this) }));
        });
        return (React.createElement("div", { className: "container" },
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm" },
                    React.createElement("input", { className: "editSearch", placeholder: "\u041F\u043E\u0438\u0441\u043A", onChange: this.onChange.bind(this) }),
                    React.createElement("div", { className: "col-sm editSourceList mt-2" },
                        React.createElement(CheckboxTree, { icons: { check: React.createElement("span", { className: "checkbox-check-ic" }), uncheck: React.createElement("span", { className: "checkbox-uncheck-ic" }) }, showNodeIcon: false, nodes: treeSource, checked: this.state.checked, expanded: this.state.expanded, onCheck: checked => this.setState({ checked }), onExpand: expanded => this.setState({ expanded }) }))),
                React.createElement("div", { className: "col-sm" },
                    React.createElement("input", { className: "editSearch", placeholder: "\u041F\u043E\u0438\u0441\u043A", onChange: this.onChangeParent.bind(this) }),
                    React.createElement("div", { className: "col-sm editSourceList mt-2" }, parentList))),
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm editListViewNavigation" },
                    React.createElement("input", { className: "btnApply", type: "button", value: "\u0413\u043E\u0442\u043E\u0432\u043E", onClick: this.apply.bind(this) })))));
    }
}
