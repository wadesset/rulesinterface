import * as React from 'react';
import { utilsConstants } from "../../constants/utilsConstants";
export default class CheckItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { checked: false };
    }
    onChange(event) {
        if (event.target.checked) {
            this.setState({ checked: !this.state.checked }, () => { this.props.addToCheckedList(this.props.id); });
        }
        else {
            this.setState({ checked: !this.state.checked }, () => { this.props.removeFromCheckedList(this.props.id); });
        }
    }
    static getDerivedStateFromProps(props, state) {
        if (props.checked) {
            return {
                checked: true
            };
        }
        else {
            return {
                checked: null
            };
        }
    }
    render() {
        const groups = [utilsConstants.COMPANY_GROUP_SYS_CODE, utilsConstants.UNIT_SYS_CODE, utilsConstants.ROLE_SYS_CODE, utilsConstants.ORGANIZATION_SYS_CODE];
        if (groups.includes(this.props.id)) {
            return (React.createElement("div", { className: `editItem ${this.props.background}` },
                React.createElement("span", { className: "itemDefaultLabel" }, this.props.name)));
        }
        else {
            return (React.createElement("div", { className: `editItem ${this.props.background}` },
                React.createElement("input", { id: `checkbox-id-${this.props.id}`, type: "checkbox", value: this.props.id, checked: !!this.state.checked, onChange: this.onChange.bind(this) }),
                React.createElement("label", { htmlFor: `checkbox-id-${this.props.id}` }),
                React.createElement("span", { className: "itemLabel" }, this.props.name)));
        }
    }
}
