import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { utilsConstants } from "../constants/utilsConstants";
import { actionConstants } from "../constants/actionConstants";
import { routeConstants } from "../constants/routeConstants";
import { openSettings, updateBox, deleteBox } from "../actions/editorActions";
import { updateAccessGroupName } from "../actions/mainActions";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
class AccessGroupSettings extends React.Component {
    constructor(props) {
        super(props);
    }
    onChange() {
        this.props.openSettings(false);
    }
    onSelect(selectedItem) {
        this.props.updateBox(selectedItem.value, this.props.parentItem.id);
    }
    onChangeName(event) {
        this.props.updateAccessGroupName(this.props.parentItem.id, event.target.value);
    }
    onDelete() {
        this.props.deleteBox(this.props.parentItem.id, null, routeConstants.DELETE_ACCESS_GROUP_BOX_URL, actionConstants.DELETE_ACCESS_GROUPS);
        this.props.openSettings(false);
    }
    render() {
        const source = this.props.accessGroupEntities.map(item => {
            return { value: item.id, label: item.name };
        });
        console.log(this.props.parentItem);
        let defaultValue = this.props.parentItem.selectedValueId === utilsConstants.DEFAULT_GUID
            ? "Choose Group"
            : this.props.parentItem.selectedValueId;
        return (React.createElement("div", { className: "mainSettingsBox" },
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm-1" },
                    React.createElement("span", { className: "mainSettingsSquareLabel" }, "Settings")),
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement("button", { className: "returnBtn ml-5", onClick: this.onChange.bind(this) },
                        React.createElement("i", { className: "back-ic" }),
                        React.createElement("span", { className: "ml-2" }, "Back")))),
            React.createElement("div", { className: "row mt-4" },
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement("span", null, "Name"),
                    React.createElement("input", { className: "settingsInput mt-1", placeholder: "Enter name", type: "text", onChange: this.onChangeName.bind(this), value: this.props.parentItem.name }))),
            React.createElement("div", { className: "row mt-2" },
                React.createElement("div", { className: "col-sm-3" },
                    React.createElement("span", null, "Choose Group"),
                    React.createElement(Dropdown, { className: "mt-1", options: source, value: defaultValue, onChange: this.onSelect.bind(this), placeholder: "Choose Group" }))),
            React.createElement("div", { className: "row mt-2" },
                React.createElement("div", { className: "col-sm-3" },
                    React.createElement("span", null, "Description"),
                    React.createElement("div", { className: "mt-1" },
                        React.createElement("textarea", { className: "settingsTextArea", placeholder: "Enter description" })))),
            React.createElement("div", { className: "row mt-2" },
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement("span", null, "Owner"),
                    React.createElement("input", { className: "settingsInput mt-1", placeholder: "Find owner", type: "text" }))),
            React.createElement("div", { className: "row mt-3" },
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement("span", null, "Group type"))),
            React.createElement("div", { className: "row mt-2" },
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement("div", { className: "groupTypeBtnWhite" }, "Active Directory")),
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement("div", { className: "groupTypeBtnRed" }, "Share Point"))),
            React.createElement("div", { className: "row mt-3" },
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement("span", null, "Activities"),
                    React.createElement("div", { className: "" },
                        React.createElement("div", { className: "button r", id: "button-9" },
                            React.createElement("input", { type: "checkbox", className: "checkbox" }),
                            React.createElement("div", { className: "knobs" },
                                React.createElement("span", null)),
                            React.createElement("div", { className: "layer" }))))),
            React.createElement("div", { className: "row mt-2" },
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement("div", { className: "deleteAccessGroupBtn", onClick: this.onDelete.bind(this) },
                        React.createElement("i", { className: "delete-ic" }),
                        React.createElement("span", { className: "ml-2" }, "Delete this group"))))));
    }
}
let mapProps = (state) => {
    return {
        parentItem: state.mainReducer.parentItem,
        accessGroupEntities: state.editorReducer.accessGroupEntities,
    };
};
let mapDispatch = (dispatch) => {
    return {
        openSettings: bindActionCreators(openSettings, dispatch),
        updateBox: bindActionCreators(updateBox, dispatch),
        deleteBox: bindActionCreators(deleteBox, dispatch),
        updateAccessGroupName: bindActionCreators(updateAccessGroupName, dispatch)
    };
};
export default connect(mapProps, mapDispatch)(AccessGroupSettings);
