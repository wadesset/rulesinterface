import * as React from 'react';
import 'react-dropdown/style.css';
import { connect } from 'react-redux';
import Settings from "../../components/Settings";
import AccessGroupSettings from "../../components/AccessGroupSettings";
import LeftMenu from "../../components/LeftMenu";
class Main extends React.Component {
    render() {
        let settings = this.props.openedSettings ? React.createElement(AccessGroupSettings, null) : React.createElement(Settings, null);
        return (React.createElement("div", { className: "container-fluid" },
            React.createElement("div", { className: "mainSettingsSquareLabel appLabel" }, "Nakovalni optom"),
            React.createElement("hr", null),
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "col-sm-2" },
                    React.createElement(LeftMenu, null)),
                React.createElement("div", { className: "col-sm-9" }, settings))));
    }
}
let mapProps = (state) => {
    return {
        openedSettings: state.editorReducer.openedSettings
    };
};
let mapDispatch = (dispatch) => {
    return {};
};
export default connect(mapProps, mapDispatch)(Main);
