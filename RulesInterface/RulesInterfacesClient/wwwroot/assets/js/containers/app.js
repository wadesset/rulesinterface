import * as React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Main from './main/main';
export default class App extends React.Component {
    render() {
        return (React.createElement(Router, null,
            React.createElement("div", { id: "document" },
                React.createElement("main", null,
                    React.createElement(Switch, null,
                        React.createElement(Route, { path: "/main", component: Main }),
                        React.createElement(Route, { exact: true, path: "/", render: () => (React.createElement(Redirect, { to: "/main" })) }))))));
    }
}
