import { actionConstants } from "../constants/actionConstants";
const initialState = {
    component: null,
    accessGroupEntities: [],
    companyGroupEntities: [],
    organizationEntities: [],
    unitEntities: [],
    roleEntities: [],
    statusEntities: [],
    allowedToManage: true,
    openedSettings: false,
    error: ''
};
export default function editorReducer(state = initialState, action) {
    switch (action.type) {
        case actionConstants.SWITCH_COMPANY_GROUPS_EDITOR:
            return Object.assign(Object.assign({}, state), { switchCompanyGroupsEditor: action.data, error: '' });
        case actionConstants.SWITCH_ORGANIZATIONS_EDITOR:
            return Object.assign(Object.assign({}, state), { switchCompanyGroupsEditor: action.data, error: '' });
        case actionConstants.SWITCH_UNITS_EDITOR:
            return Object.assign(Object.assign({}, state), { switchCompanyGroupsEditor: action.data, error: '' });
        case actionConstants.SWITCH_ROLES_EDITOR:
            return Object.assign(Object.assign({}, state), { switchCompanyGroupsEditor: action.data, error: '' });
        case actionConstants.GET_ACCESS_GROUP_ENTITY:
            return Object.assign(Object.assign({}, state), { accessGroupEntities: action.data, error: '' });
        case actionConstants.GET_COMPANY_GROUP_ENTITY:
            return Object.assign(Object.assign({}, state), { companyGroupEntities: action.data, error: '' });
        case actionConstants.GET_ORGANIZATION_ENTITY:
            return Object.assign(Object.assign({}, state), { organizationEntities: action.data, error: '' });
        case actionConstants.GET_UNIT_ENTITY:
            return Object.assign(Object.assign({}, state), { unitEntities: action.data, error: '' });
        case actionConstants.GET_ROLE_ENTITY:
            return Object.assign(Object.assign({}, state), { roleEntities: action.data, error: '' });
        case actionConstants.GET_STATUS_ENTITY:
            return Object.assign(Object.assign({}, state), { statusEntities: action.data, error: '' });
        case actionConstants.ALLOWED_TO_MANAGE:
            return Object.assign(Object.assign({}, state), { allowedToManage: action.data, error: '' });
        case actionConstants.OPEN_SETTINGS:
            return Object.assign(Object.assign({}, state), { openedSettings: action.data, error: '' });
        default:
            return state;
    }
}
