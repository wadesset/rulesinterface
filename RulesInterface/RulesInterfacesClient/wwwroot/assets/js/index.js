import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from "./configureStore";
import App from './containers/app';
import 'bootstrap/dist/css/bootstrap.min.css';
const store = configureStore();
render(React.createElement(Provider, { store: store },
    React.createElement(App, null)), document.getElementById('content'));
