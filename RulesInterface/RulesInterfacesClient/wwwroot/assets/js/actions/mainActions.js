var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import "isomorphic-fetch";
import { actionConstants } from "../constants/actionConstants";
import { routeConstants } from '../constants/routeConstants';
export function getFullTree(parentId) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        let queryTrailer = "?parentId=" + parentId;
        yield fetch(routeConstants.PATH_URL + routeConstants.GET_BY_ACCESS_GROUP_BOXES_URL + queryTrailer)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.GET_FULL_TREE, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    });
}
export function getAccessGroups() {
    return (dispatch) => {
        fetch(routeConstants.PATH_URL + routeConstants.GET_ACCESS_GROUP_BOXES_URL)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.GET_ACCESS_GROUP_BOXES, accessGroups: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}
export function updateAccessGroupName(id, value) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + routeConstants.UPDATE_NAME_ACCESS_GROUP_BOX_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ id: id, value: value })
        })
            .then((response) => { return response.json(); })
            .then(data => dispatch({ type: actionConstants.UPDATE_NAME_ACCESS_GROUP_BOX, data: data, parentId: id }))
            .catch((ex) => { dispatch({ type: actionConstants.UPDATE_NAME_ACCESS_GROUP_BOX, error: ex }); });
    });
}
export function createAccessGroup() {
    return (dispatch) => {
        console.log(routeConstants.PATH_URL + routeConstants.CREATE_ACCESS_GROUP_BOX_URL);
        fetch(routeConstants.PATH_URL + routeConstants.CREATE_ACCESS_GROUP_BOX_URL)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.CREATE_ACCESS_GROUP_BOX, accessGroups: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}
export function selectParent(selectedValueId, route) {
    if (selectedValueId !== null) {
        return (dispatch) => __awaiter(this, void 0, void 0, function* () {
            yield dispatch({ type: route, id: selectedValueId });
        });
    }
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield dispatch({ type: actionConstants.DEFAULT_STATE });
    });
}
export function setDefaultStates() {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield dispatch({ type: actionConstants.DEFAULT_STATE });
    });
}
export function setParentItem(id) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield dispatch({ type: actionConstants.SET_PARENT_ITEM, data: id });
    });
}
