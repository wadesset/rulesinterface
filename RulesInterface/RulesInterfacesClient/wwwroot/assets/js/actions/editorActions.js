var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import "isomorphic-fetch";
import { actionConstants } from "../constants/actionConstants";
import { routeConstants } from '../constants/routeConstants';
export function updateBox(selectedItem, boxId) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + routeConstants.UPDATE_ACCESS_GROUP_BOX_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ selectedValueId: selectedItem, boxId: boxId })
        })
            .then((response) => { return response.json(); })
            .then(data => dispatch({ type: actionConstants.UPDATE_ACCESS_GROUP_BOX, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.UPDATE_ACCESS_GROUP_BOX, error: ex }); });
    });
}
export function createBox(selectedItems, parentBoxId, accessGroupId = null, route, reducerRoute) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + route, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ selectedValueIds: selectedItems, parentBoxId: parentBoxId, accessGroupId: accessGroupId })
        })
            .then((response) => { return response.json(); })
            .then(data => dispatch({ type: reducerRoute, data: data }))
            .catch((ex) => { dispatch({ type: reducerRoute, error: ex }); });
    });
}
export function deleteBox(currentBoxId, accessGroupId = null, route, reducerRoute) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + route, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ currentBoxId: currentBoxId, accessGroupId: accessGroupId })
        })
            .then((response) => { return response.json(); })
            .then(data => dispatch({ type: reducerRoute, data: data }))
            .catch((ex) => { dispatch({ type: reducerRoute, error: ex }); });
    });
}
export function getAccessGroupEntities() {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + routeConstants.GET_ACCESS_GROUP_ENTITY)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.GET_ACCESS_GROUP_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    });
}
export function getCompanyGroupEntities() {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + routeConstants.GET_COMPANY_GROUP_ENTITY)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.GET_COMPANY_GROUP_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    });
}
export function getOrganizationEntities() {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + routeConstants.GET_ORGANIZATION_ENTITY)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.GET_ORGANIZATION_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    });
}
export function getUnitEntities() {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + routeConstants.GET_UNIT_ENTITY)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.GET_UNIT_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    });
}
export function getRoleEntities() {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + routeConstants.GET_ROLE_ENTITY)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.GET_ROLE_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    });
}
export function getStatusEntities() {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield fetch(routeConstants.PATH_URL + routeConstants.GET_STATUS_ENTITY)
            .then((response) => {
            return response.json();
        })
            .then(data => dispatch({ type: actionConstants.GET_STATUS_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    });
}
export function switchEditor(route, value) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield dispatch({ type: route, data: value });
    });
}
export function allowedToManage(value) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield dispatch({ type: actionConstants.ALLOWED_TO_MANAGE, data: value });
    });
}
export function openSettings(value) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        yield dispatch({ type: actionConstants.OPEN_SETTINGS, data: value });
    });
}
