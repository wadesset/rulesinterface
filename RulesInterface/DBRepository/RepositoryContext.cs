﻿using System;
using System.Linq;
using Models;
using Microsoft.EntityFrameworkCore;

namespace DBRepository
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions<RepositoryContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();
            return base.SaveChanges();
        }

        private void updateUpdatedProperty<T>() where T : class
        {
            var modifiedSourceInfo =
                ChangeTracker.Entries<T>()
                    .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified);

            foreach (var entry in modifiedSourceInfo)
            {
                entry.Property("UpdatedTimestamp").CurrentValue = DateTime.UtcNow;
            }
        }

        public DbSet<AccessGroup> AccessGroup { get; set; }
        public DbSet<AccessGroupBox> AccessGroupBox { get; set; }
        public DbSet<CompanyGroup> CompanyGroup { get; set; }
        public DbSet<CompanyGroupBox> CompanyGroupBox { get; set; }
        public DbSet<Organization> Organization { get; set; }
        public DbSet<OrganizationBox> OrganizationBox { get; set; }
        public DbSet<Unit> Unit { get; set; }
        public DbSet<UnitBox> UnitBox { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleBox> RoleBox { get; set; }
    }
}
