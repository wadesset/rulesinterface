﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBRepository.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DBRepository.Repositories
{
    public class MainRepository : BaseRepository, IMainRepository
    {
        public MainRepository(string connectionString, IRepositoryContextFactory contextFactory) : base(connectionString, contextFactory)
        {
        }

        /// <summary>
        /// Get all Access Groups
        /// </summary>
        /// <returns></returns>
        public async Task<List<AccessGroup>> GetAccessGroups()
        {
            List<AccessGroup> result;

            using (var context = ContextFactory.CreateDbContext(ConnectionString))
            {
                var query = context.AccessGroup.AsQueryable();
                result = await query.ToListAsync();
            }

            return result;
        }
    }
}
