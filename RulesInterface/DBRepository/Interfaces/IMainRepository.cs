﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DBRepository.Interfaces
{
    interface IMainRepository
    {
        Task<List<Models.AccessGroup>> GetAccessGroups();
    }
}
