﻿using System.Data.Entity;
using RulesInterfaceTest.Models;

namespace RulesInterfaceTest.Context
{
    public class RulesInterfaceContext : DbContext
    {
        public RulesInterfaceContext()
            : base("name=RulesInterfaceContext")
        {
        }

        public virtual DbSet<AccessGroupBox> AccessGroupBoxes { get; set; }
        public virtual DbSet<AccessGroup> AccessGroups { get; set; }
        public virtual DbSet<CompanyGroupBox> CompanyGroupBoxes { get; set; }
        public virtual DbSet<CompanyGroup> CompanyGroups { get; set; }
        public virtual DbSet<OrganizationBox> OrganizationBoxes { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<UnitBox> UnitBoxes { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<RoleBox> RoleBoxes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<AccessGroupRelation> Relations { get; set; }
    }
}