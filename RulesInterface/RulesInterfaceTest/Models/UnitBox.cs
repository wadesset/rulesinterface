﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RulesInterfaceTest.Models
{
    public class UnitBox
    {
        [Key]
        public Guid Id { get; set; }
    }
}
