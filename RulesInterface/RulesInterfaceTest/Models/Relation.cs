﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RulesInterfaceTest.Models
{
    public class AccessGroupRelation
    {
        [Key]
        public Guid Id { get; set; }
        public AccessGroup AccessGroup { get; set; }
    }
}
