﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RulesInterfaceTest.Models
{
    public class RoleBox
    {
        [Key]
        public Guid Id { get; set; }
    }
}
