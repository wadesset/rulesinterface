﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RulesInterfaceTest.Models
{
    public class CompanyGroupBox
    {
        [Key]
        public Guid Id { get; set; }
    }
}
