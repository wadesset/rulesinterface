﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RulesInterfaceTest.Models
{
    public class OrganizationBox
    {
        [Key]
        public Guid Id { get; set; }
    }
}
