﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RulesInterfaceTest.Models
{
    public class AccessGroupBox
    {
        [Key]
        public Guid Id { get; set; }
        public List<AccessGroupRelation> Relations { get; set; }
    }
}
