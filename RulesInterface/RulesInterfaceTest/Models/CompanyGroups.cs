﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RulesInterfaceTest.Models
{
    public class CompanyGroup
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
