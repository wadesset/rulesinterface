﻿using System;

namespace RulesInterfaceAPI.ViewModels
{
    public class BoxView
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ParentBoxId { get; set; }
        public Guid SelectedValueId { get; set; }
    }
}
