﻿using System;
using System.Linq;
using System.Web.Mvc;
using RulesInterfaceAPI.Context;
using RulesInterfaceAPI.Models;

namespace RulesInterfaceAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
