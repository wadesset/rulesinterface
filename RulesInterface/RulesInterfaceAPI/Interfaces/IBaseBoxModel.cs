﻿using RulesInterfaceAPI.Models;
using System;
using System.Collections.Generic;

namespace RulesInterfaceAPI.Interfaces
{
    public interface IBaseBoxModel
    {
        Guid Id { get; set; }
        Guid ParentBoxId { get; set; }
        Guid SelectedValueId { get; set; }
    }
}
