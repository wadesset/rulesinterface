﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RulesInterfaceAPI.Interfaces;

namespace RulesInterfaceAPI.Models
{
    public class Unit : IBaseModel
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? GeneralParentId { get; set; }
    }
}
