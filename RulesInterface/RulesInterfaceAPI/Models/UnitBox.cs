﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RulesInterfaceAPI.Interfaces;

namespace RulesInterfaceAPI.Models
{
    public class UnitBox : ITreeBoxModel
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ParentBoxId { get; set; }
        public Guid SelectedValueId { get; set; }
        public Guid? InnerParentId { get; set; }
    }
}
