﻿using System;
using System.ComponentModel.DataAnnotations;
using RulesInterfaceAPI.Interfaces;

namespace RulesInterfaceAPI.Models
{
    public class CompanyGroupBox : IBaseBoxModel
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ParentBoxId { get; set; }
        public Guid SelectedValueId { get; set; }
    }
}
