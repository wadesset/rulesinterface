﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RulesInterfaceAPI.Interfaces;
using RulesInterfaceAPI.Models;

namespace UnitTestRulesInterface.Models
{
    public class AccessGroupBox : IBaseBoxModel
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ParentBoxId { get; set; }
        public Guid SelectedValueId { get; set; }

        public Guid SelectedValueRelationId { get; set; }
        //public List<Guid> SelectedValueId { get; set; }

        public virtual List<SelectedValues> SelectedValueIds { get; set; }
        //public Guid[] SelectedValueId { get; set; }
    }
}
