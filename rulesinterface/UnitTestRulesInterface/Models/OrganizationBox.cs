﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RulesInterfaceAPI.Interfaces;
using RulesInterfaceAPI.Models;

namespace UnitTestRulesInterface.Models
{
    public class OrganizationBox : IBaseBoxModel
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ParentBoxId { get; set; }
        public Guid SelectedValueId { get; set; }

        public Guid SelectedValueRelationId { get; set; }
        //public List<Guid> SelectedValueId { get; set; }

        [ForeignKey("SelectedValueRelationId")]
        public virtual List<SelectedValues> SelectedValueIds { get; set; }
        //public Guid[] SelectedValueId { get; set; }
    }
}
