﻿using System;
using System.ComponentModel.DataAnnotations;
using RulesInterfaceAPI.Interfaces;

namespace UnitTestRulesInterface.Models
{
    public class Role : IBaseModel
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? GeneralParentId { get; set; }
    }
}
