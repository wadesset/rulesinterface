﻿using System.Data.Entity;
using RulesInterfaceAPI.Models;
using AccessGroup = UnitTestRulesInterface.Models.AccessGroup;
using AccessGroupBox = UnitTestRulesInterface.Models.AccessGroupBox;
using CompanyGroup = UnitTestRulesInterface.Models.CompanyGroup;
using CompanyGroupBox = UnitTestRulesInterface.Models.CompanyGroupBox;
using Organization = UnitTestRulesInterface.Models.Organization;
using OrganizationBox = UnitTestRulesInterface.Models.OrganizationBox;
using Role = UnitTestRulesInterface.Models.Role;
using RoleBox = UnitTestRulesInterface.Models.RoleBox;
using State = UnitTestRulesInterface.Models.State;
using StateBox = UnitTestRulesInterface.Models.StateBox;
using Unit = UnitTestRulesInterface.Models.Unit;
using UnitBox = UnitTestRulesInterface.Models.UnitBox;

namespace UnitTestRulesInterface
{
    public class RulesInterfaceContext : DbContext
    {
        public RulesInterfaceContext()
            : base("name=RulesInterfaceContext")
        {
        }

        public virtual DbSet<AccessGroupBox> AccessGroupBoxes { get; set; }
        public virtual DbSet<AccessGroup> AccessGroups { get; set; }
        public virtual DbSet<CompanyGroupBox> CompanyGroupBoxes { get; set; }
        public virtual DbSet<CompanyGroup> CompanyGroups { get; set; }
        public virtual DbSet<OrganizationBox> OrganizationBoxes { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<UnitBox> UnitBoxes { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<RoleBox> RoleBoxes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<StateBox> StatesBoxes { get; set; }
    }
}