﻿using System;
using System.Collections.Generic;

namespace RulesInterfaceAPI.ViewModels
{
    public class FullTreeView
    {
        public Guid Id { get; set; }
        public List<BoxView> CompanyGroups { get; set; }
        public List<BoxView> Organizations { get; set; }
        public List<BoxView> Units { get; set; }
        public List<BoxView> Roles { get; set; }
        public List<BoxView> Statuses { get; set; }
    }
}
