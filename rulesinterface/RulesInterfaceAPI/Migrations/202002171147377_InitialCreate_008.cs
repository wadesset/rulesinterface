namespace RulesInterfaceAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_008 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccessGroupBoxes", "Name", c => c.String());
            DropColumn("dbo.Units", "InnerParentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Units", "InnerParentId", c => c.Guid());
            DropColumn("dbo.AccessGroupBoxes", "Name");
        }
    }
}
