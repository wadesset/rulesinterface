namespace RulesInterfaceAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_005 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Units", "InnerParentId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Units", "InnerParentId");
        }
    }
}
