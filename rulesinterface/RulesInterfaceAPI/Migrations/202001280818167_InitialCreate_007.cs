namespace RulesInterfaceAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_007 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Accessses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Accessses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SelectedValueId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
