namespace RulesInterfaceAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_006 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accessses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SelectedValueId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Accessses");
        }
    }
}
