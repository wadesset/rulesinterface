namespace RulesInterfaceAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_004 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UnitBoxes", "InnerParentId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UnitBoxes", "InnerParentId");
        }
    }
}
