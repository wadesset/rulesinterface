namespace RulesInterfaceAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_002 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SelectedValues", "BoxId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SelectedValues", "BoxId");
        }
    }
}
