namespace RulesInterfaceAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_003 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccessGroupBoxes", "SelectedValueId", c => c.Guid(nullable: false));
            AddColumn("dbo.CompanyGroupBoxes", "SelectedValueId", c => c.Guid(nullable: false));
            AddColumn("dbo.OrganizationBoxes", "SelectedValueId", c => c.Guid(nullable: false));
            AddColumn("dbo.RoleBoxes", "SelectedValueId", c => c.Guid(nullable: false));
            AddColumn("dbo.StateBoxes", "SelectedValueId", c => c.Guid(nullable: false));
            AddColumn("dbo.UnitBoxes", "SelectedValueId", c => c.Guid(nullable: false));
            DropColumn("dbo.AccessGroupBoxes", "SelectedValueRelationId");
            DropColumn("dbo.CompanyGroupBoxes", "SelectedValueRelationId");
            DropColumn("dbo.RoleBoxes", "SelectedValueRelationId");
            DropColumn("dbo.StateBoxes", "SelectedValueRelationId");
            DropColumn("dbo.UnitBoxes", "SelectedValueRelationId");
            DropTable("dbo.SelectedValues");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SelectedValues",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BoxId = c.Guid(nullable: false),
                        SelectedValueId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.UnitBoxes", "SelectedValueRelationId", c => c.Guid(nullable: false));
            AddColumn("dbo.StateBoxes", "SelectedValueRelationId", c => c.Guid(nullable: false));
            AddColumn("dbo.RoleBoxes", "SelectedValueRelationId", c => c.Guid(nullable: false));
            AddColumn("dbo.CompanyGroupBoxes", "SelectedValueRelationId", c => c.Guid(nullable: false));
            AddColumn("dbo.AccessGroupBoxes", "SelectedValueRelationId", c => c.Guid(nullable: false));
            DropColumn("dbo.UnitBoxes", "SelectedValueId");
            DropColumn("dbo.StateBoxes", "SelectedValueId");
            DropColumn("dbo.RoleBoxes", "SelectedValueId");
            DropColumn("dbo.OrganizationBoxes", "SelectedValueId");
            DropColumn("dbo.CompanyGroupBoxes", "SelectedValueId");
            DropColumn("dbo.AccessGroupBoxes", "SelectedValueId");
        }
    }
}
