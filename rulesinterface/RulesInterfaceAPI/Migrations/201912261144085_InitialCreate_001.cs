namespace RulesInterfaceAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_001 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccessGroupBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentBoxId = c.Guid(nullable: false),
                        SelectedValueRelationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccessGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ParentId = c.Guid(),
                        GeneralParentId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CompanyGroupBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentBoxId = c.Guid(nullable: false),
                        SelectedValueRelationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CompanyGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ParentId = c.Guid(),
                        GeneralParentId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrganizationBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentBoxId = c.Guid(nullable: false),
                        SelectedValueRelationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ParentId = c.Guid(),
                        GeneralParentId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoleBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentBoxId = c.Guid(nullable: false),
                        SelectedValueRelationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ParentId = c.Guid(),
                        GeneralParentId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SelectedValues",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SelectedValueId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ParentId = c.Guid(),
                        GeneralParentId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentBoxId = c.Guid(nullable: false),
                        SelectedValueRelationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UnitBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentBoxId = c.Guid(nullable: false),
                        SelectedValueRelationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ParentId = c.Guid(),
                        GeneralParentId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Units");
            DropTable("dbo.UnitBoxes");
            DropTable("dbo.StateBoxes");
            DropTable("dbo.States");
            DropTable("dbo.SelectedValues");
            DropTable("dbo.Roles");
            DropTable("dbo.RoleBoxes");
            DropTable("dbo.Organizations");
            DropTable("dbo.OrganizationBoxes");
            DropTable("dbo.CompanyGroups");
            DropTable("dbo.CompanyGroupBoxes");
            DropTable("dbo.AccessGroups");
            DropTable("dbo.AccessGroupBoxes");
        }
    }
}
