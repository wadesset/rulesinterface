﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RulesInterfaceAPI.Models
{
    public class SelectedValues
    {
        [Key]
        public Guid Id { get; set; }
        public Guid BoxId { get; set; }
        public Guid SelectedValueId { get; set; }
    }
}