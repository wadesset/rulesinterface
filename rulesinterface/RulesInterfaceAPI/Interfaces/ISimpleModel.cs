﻿using System;

namespace RulesInterfaceAPI.Interfaces
{
    public interface ISimpleModel
    {
        Guid Id { get; set; }
        string Name { get; set; }
        Guid ParentId { get; set; }
    }
}
