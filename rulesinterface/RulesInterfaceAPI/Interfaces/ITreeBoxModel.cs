﻿using System;

namespace RulesInterfaceAPI.Interfaces
{
    public interface ITreeBoxModel : IBaseBoxModel
    {
        Guid? InnerParentId { get; set; }
    }
}
