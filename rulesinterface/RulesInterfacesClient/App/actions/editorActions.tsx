﻿import "isomorphic-fetch";
import { actionConstants } from "../constants/actionConstants";
import { routeConstants } from '../constants/routeConstants';

export function updateBox(selectedItem, boxId) {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + routeConstants.UPDATE_ACCESS_GROUP_BOX_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ selectedValueId: selectedItem, boxId: boxId })
            })
            .then((response) => { return response.json(); })
            .then(data => dispatch({ type: actionConstants.UPDATE_ACCESS_GROUP_BOX, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.UPDATE_ACCESS_GROUP_BOX, error: ex }); });
    };
}

export function createBox(selectedItems, parentBoxId, accessGroupId = null, route, reducerRoute) {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + route, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ selectedValueIds: selectedItems, parentBoxId: parentBoxId, accessGroupId: accessGroupId })
        })
            .then((response) => { return response.json(); })
            .then(data => dispatch({ type: reducerRoute, data: data }))
            .catch((ex) => { dispatch({ type: reducerRoute, error: ex }); });
    };
}

export function deleteBox(currentBoxId, accessGroupId = null, route, reducerRoute) {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + route, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ currentBoxId: currentBoxId, accessGroupId: accessGroupId })
        })
            .then((response) => { return response.json(); })
            .then(data => dispatch({ type: reducerRoute, data: data }))
            .catch((ex) => { dispatch({ type: reducerRoute, error: ex }); });
    };
}

export function getAccessGroupEntities() {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + routeConstants.GET_ACCESS_GROUP_ENTITY)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.GET_ACCESS_GROUP_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function getCompanyGroupEntities() {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + routeConstants.GET_COMPANY_GROUP_ENTITY)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.GET_COMPANY_GROUP_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function getOrganizationEntities() {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + routeConstants.GET_ORGANIZATION_ENTITY)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.GET_ORGANIZATION_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function getUnitEntities() {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + routeConstants.GET_UNIT_ENTITY)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.GET_UNIT_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function getRoleEntities() {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + routeConstants.GET_ROLE_ENTITY)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.GET_ROLE_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function getStatusEntities() {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + routeConstants.GET_STATUS_ENTITY)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.GET_STATUS_ENTITY, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function switchEditor(route, value) {
    return async (dispatch) => {
        await dispatch({ type: route, data: value });
    };
}

export function allowedToManage(value) {
    return async (dispatch) => {
        await dispatch({ type: actionConstants.ALLOWED_TO_MANAGE, data: value });
    };
}

export function openSettings(value) {
    return async (dispatch) => {
        await dispatch({ type: actionConstants.OPEN_SETTINGS, data: value });
    };
}