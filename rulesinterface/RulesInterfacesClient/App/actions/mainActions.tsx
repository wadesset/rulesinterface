﻿import "isomorphic-fetch";
import { actionConstants } from "../constants/actionConstants";
import { routeConstants } from '../constants/routeConstants';

export function getFullTree(parentId) {
    return async (dispatch) => {
        let queryTrailer = "?parentId=" + parentId;
        await fetch(routeConstants.PATH_URL + routeConstants.GET_BY_ACCESS_GROUP_BOXES_URL + queryTrailer)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.GET_FULL_TREE, data: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function getAccessGroups() {
    return (dispatch) => {
        fetch(routeConstants.PATH_URL + routeConstants.GET_ACCESS_GROUP_BOXES_URL)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.GET_ACCESS_GROUP_BOXES, accessGroups: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function updateAccessGroupName(id, value) {
    return async (dispatch) => {
        await fetch(routeConstants.PATH_URL + routeConstants.UPDATE_NAME_ACCESS_GROUP_BOX_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ id: id, value: value })
            })
            .then((response) => { return response.json(); })
            .then(data => dispatch({ type: actionConstants.UPDATE_NAME_ACCESS_GROUP_BOX, data: data, parentId: id }))
            .catch((ex) => { dispatch({ type: actionConstants.UPDATE_NAME_ACCESS_GROUP_BOX, error: ex }); });
    };
}

export function createAccessGroup() {
    return (dispatch) => {
        console.log(routeConstants.PATH_URL + routeConstants.CREATE_ACCESS_GROUP_BOX_URL);
        fetch(routeConstants.PATH_URL + routeConstants.CREATE_ACCESS_GROUP_BOX_URL)
            .then((response) => {
                return response.json();
            })
            .then(data => dispatch({ type: actionConstants.CREATE_ACCESS_GROUP_BOX, accessGroups: data }))
            .catch((ex) => { dispatch({ type: actionConstants.ERROR, error: ex }); });
    };
}

export function selectParent(selectedValueId, route) {
    if (selectedValueId !== null) {
        return async (dispatch) => {
            await dispatch({ type: route, id: selectedValueId });
        };
    } 

    return async (dispatch) => {
        await dispatch({ type: actionConstants.DEFAULT_STATE });
    };
}

export function setDefaultStates() {
    return async (dispatch) => {
        await dispatch({ type: actionConstants.DEFAULT_STATE });
    };
}

export function setParentItem(id) {
    return async (dispatch) => {
        await dispatch({ type: actionConstants.SET_PARENT_ITEM, data: id });
    };
}