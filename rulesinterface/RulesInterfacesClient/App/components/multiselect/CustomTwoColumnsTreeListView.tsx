﻿import * as React from 'react';
import { IModelProps as BoxEntityView, IBoxProps as BoxView, ITreeData as TreeBox } from "../../interfaces/irules";
import CheckItem from "./CheckItem";
import CheckboxTree from 'react-checkbox-tree';
import 'react-checkbox-tree/lib/react-checkbox-tree.css';


interface IProps {
    source: BoxEntityView[];
    parentSource: BoxView[];
    apply: (list: string[], parentId: string) => void;
}

interface IState {
    list: BoxEntityView[];
    parentList: BoxView[];
    checkedParentList: string[];
    checked: string[];
    expanded: string[];
}

export default class CustomTwoColumnsTreeListView extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            checkedParentList: [],
            parentList: this.props.parentSource,
            checked: [],
            expanded: []
        };
    }

    apply() {
        this.props.apply(this.state.checked, this.state.checkedParentList[0]);
    }

    onChange(event) {
        this.setState({ list: this.props.source.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }

    onChangeParent(event) {
        this.setState({ parentList: this.props.parentSource.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }

    addToCheckedParentList(id) {
        this.setState({ checkedParentList: this.state.checkedParentList.concat(id) });
        this.setState({ list: this.props.source.filter(f => f.generalParentId === this.props.parentSource.filter(f => f.id === id)[0].selectedValueId) });
    }

    removeFromCheckedParentList(id) {
        let list = this.state.checkedParentList;
        const index = list.indexOf(id);
        list.splice(index, 1);
        this.setState({ checkedParentList: list });
        this.setState({ list: [] });
    }

    onChangeTree(checked, targetNode) {
        if (!targetNode.checked) {
            this.setState({ checked: this.state.checked.concat(targetNode.value) });
        } else {
            let list = this.state.checked;
            const index = list.indexOf(targetNode.value);
            list.splice(index, 1);
            this.setState({ checked: list });
        }
    }

    render() {
        const treeSource = this.state.list ? this.state.list.filter(f => f.parentId === null).map(item => {
            let j = this.state.list.filter((f) => { return f.parentId === item.id });
            if (j !== undefined && j.length > 0) {
                return {
                    label: item.name,
                    value: item.id,
                    className: "treeNodeText ",
                    children: this.state.list.filter((f) => { return f.parentId === item.id }).map(child => {
                        return {
                            label: child.name,
                            value: child.id,
                            className: "treeNodeText ",
                        }
                    })
                }
            } else {
                let background = this.state.list.filter(f => f.parentId === null).indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
                return {
                    label: item.name,
                    value: item.id,
                    className: "treeNodeText " + background,
                }
            }
        }) : [];

        let parentList = this.state.parentList.map(item => {
            let checked = this.state.checkedParentList.includes(item.id);
            let background = this.props.parentSource.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            return (
                <CheckItem key={item.id} id={item.id} name={item.name} checked={checked} background={background} addToCheckedList={this.addToCheckedParentList.bind(this)} removeFromCheckedList={this.removeFromCheckedParentList.bind(this)} />
            );
        });

        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                        <input className="editSearch" placeholder="Search" onChange={this.onChange.bind(this)} />
                        <div className="col-sm editSourceList mt-2">
                            <CheckboxTree icons={{ check: <span className="checkbox-check-ic" />, uncheck: <span className="checkbox-uncheck-ic" />}} showNodeIcon={false} nodes={treeSource} checked={this.state.checked} expanded={this.state.expanded} onCheck={checked => this.setState({ checked })} onExpand={expanded => this.setState({ expanded })} />
                        </div>
                    </div>
                    <div className="col-sm">
                        <input className="editSearch" placeholder="Search" onChange={this.onChangeParent.bind(this)} />
                        <div className="col-sm editSourceList mt-2">
                            {parentList}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm editListViewNavigation">
                        <input className="btnApply" type="button" value="Apply" onClick={this.apply.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }
}