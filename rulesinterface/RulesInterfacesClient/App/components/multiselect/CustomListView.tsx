﻿import * as React from 'react';
import { IModelProps as BoxEntityView } from "../../interfaces/irules";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import CheckItem from "./CheckItem";

interface IProps {
    source: BoxEntityView[];
    selectedItems: string[];
    apply: (list: string[])=> void;
}

interface IState {
    list: BoxEntityView[];
    checkedList: string[];
}

export default class CustomListView extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = { list: this.props.source, checkedList: this.props.selectedItems };
    }

    static defaultProps = {
        selectedItems: []
    } as Partial<IProps>;

    apply() {
        this.props.apply(this.state.checkedList);
    }

    onChange(event) {
        this.setState({ list: this.props.source.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }

    checkAll() {
        this.setState({ checkedList: this.state.list.map(m => m.id) });
    }

    uncheckAll() {
        this.setState({ checkedList: [] });
    }

    addToCheckedList(id) {
        this.setState({ checkedList: this.state.checkedList.concat(id) });
    }

    removeFromCheckedList(id) {
        let list = this.state.checkedList;
        const index = list.indexOf(id);
        list.splice(index, 1);
        this.setState({ checkedList: list });
    }

    render() {
        let list = this.state.list ? this.state.list.map(item => {
            let checked = this.state.checkedList.includes(item.id);
            let background = this.props.source.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            return (
                <CheckItem key={item.id} id={item.id} name={item.name} checked={checked} background={background} addToCheckedList={this.addToCheckedList.bind(this)} removeFromCheckedList={this.removeFromCheckedList.bind(this)} />
            );
        }) : [];

        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                        <input className="editSearch" placeholder="Search" onChange={this.onChange.bind(this)} />
                        <div className="col-sm editSourceList mt-2">
                            {list}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm editListViewNavigation">
                        <input className="btnApply" type="button" value="Apply" onClick={this.apply.bind(this)} />
                        <input className="btnApplyAll" type="button" onClick={this.checkAll.bind(this)} value="All" />
                        <input className="btnCancelAll" type="button" onClick={this.uncheckAll.bind(this)} value="Take off all" />
                    </div>
                </div>
                </div>
        );
    }
}