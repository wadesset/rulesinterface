﻿import * as React from 'react';
import { IModelProps as BoxEntityView, IBoxProps as BoxView } from "../../interfaces/irules";
import CheckItem from "./CheckItem";

interface IProps {
    source: BoxEntityView[];
    parentSource: BoxView[];
    selectedItems: string[];
    visibleSelectAllBtn: boolean;
    visibleUnselectAllBtn: boolean;
    apply: (list: string[], parentId: string) => void;
}

interface IState {
    list: BoxEntityView[];
    parentList: BoxView[];
    checkedList: string[];
    checkedParentList: string[];
}

export default class CustomTwoColumnsListView extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = { list: this.props.source, checkedList: this.props.selectedItems, checkedParentList: [], parentList: this.props.parentSource };
    }

    static defaultProps = {
        selectedItems: [],
        visibleSelectAllBtn: true,
        visibleUnselectAllBtn: true
    } as Partial<IProps>;

    apply() {
        this.props.apply(this.state.checkedList, this.state.checkedParentList[0]);
    }

    onChange(event) {
        this.setState({ list: this.props.source.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }

    onChangeParent(event) {
        this.setState({ parentList: this.props.parentSource.filter(f => f.name.toLowerCase().includes(event.target.value.toLowerCase())) });
    }

    checkAll() {
        this.setState({ checkedList: this.state.list.map(m => m.id) });
    }

    uncheckAll() {
        this.setState({ checkedList: [] });
    }

    addToCheckedList(id) {
        this.setState({ checkedList: this.state.checkedList.concat(id)});
    }

    removeFromCheckedList(id) {
        let list = this.state.checkedList;
        const index = list.indexOf(id);
        list.splice(index, 1);
        this.setState({ checkedList: list });
    }

    addToCheckedParentList(id) {
        this.setState({ checkedParentList: this.state.checkedParentList.concat(id)});
    }

    removeFromCheckedParentList(id) {
        let list = this.state.checkedParentList;
        const index = list.indexOf(id);
        list.splice(index, 1);
        this.setState({ checkedParentList: list });
    }

    render() {
        let list = this.state.list ? this.state.list.map(item => {
            let checked = this.state.checkedList.includes(item.id);
            let background = this.props.source.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            return (
                <CheckItem key={item.id} id={item.id} name={item.name} checked={checked} background={background} addToCheckedList={this.addToCheckedList.bind(this)} removeFromCheckedList={this.removeFromCheckedList.bind(this)} />
            );
        }) : [];

        let parentList = this.state.parentList.map(item => {
            let checked = this.state.checkedParentList.includes(item.id);
            let background = this.props.parentSource.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            return (
                <CheckItem key={item.id} id={item.id} name={item.name} checked={checked} background={background} addToCheckedList={this.addToCheckedParentList.bind(this)} removeFromCheckedList={this.removeFromCheckedParentList.bind(this)} />
            );
        });

        let btnApplyAll = this.props.visibleSelectAllBtn ? <input className="btnApplyAll" type="button" onClick={this.checkAll.bind(this)} value="Выбрать все" /> : null;
        let btnCancelAll = this.props.visibleUnselectAllBtn ? <input className="btnCancelAll" type="button" onClick={this.uncheckAll.bind(this)} value="Снять выделение" /> : null;

        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                        <input className="editSearch" placeholder="Search" onChange={this.onChange.bind(this)} />
                        <div className="col-sm editSourceList mt-2">
                            {list}
                        </div>
                    </div>
                    <div className="col-sm">
                        <input className="editSearch" placeholder="Search" onChange={this.onChangeParent.bind(this)} />
                        <div className="col-sm editSourceList mt-2">
                            {parentList}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm editListViewNavigation">
                        <input className="btnApply" type="button" value="Apply" onClick={this.apply.bind(this)}/>
                        {btnApplyAll}
                        {btnCancelAll}
                    </div>
                </div>
            </div>
        );
    }
}