﻿import * as React from 'react';
import { utilsConstants } from "../../constants/utilsConstants";

interface IProps {
    id: string;
    name: string;
    background: string;
    checked: boolean;
    addToCheckedList: (id: string) => void;
    removeFromCheckedList: (id: string) => void;
}

interface IState {
    checked: boolean;
}

export default class CheckItem extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = { checked: false };
    }

    onChange(event) {
        if (event.target.checked) {
            this.setState({ checked: !this.state.checked }, () => { this.props.addToCheckedList(this.props.id) });
        } else {
            this.setState({ checked: !this.state.checked }, () => { this.props.removeFromCheckedList(this.props.id) });
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.checked) {
            return {
                checked: true
            }
        } else {
            return {
                checked: null
            };
        }
    }

    render() {
        const groups = [ utilsConstants.COMPANY_GROUP_SYS_CODE, utilsConstants.UNIT_SYS_CODE, utilsConstants.ROLE_SYS_CODE, utilsConstants.ORGANIZATION_SYS_CODE ];

        if (groups.includes(this.props.id)) {
            return (
                <div className={`editItem ${this.props.background}`}>
                    <span className="itemDefaultLabel">{this.props.name}</span>
                </div>
            );
        } else {
            return (
                <div className={`editItem ${this.props.background}`}>
                    <input id={`checkbox-id-${this.props.id}`} type="checkbox" value={this.props.id} checked={!!
                    this.state.checked} onChange={this.onChange.bind(this)}/>
                    <label htmlFor={`checkbox-id-${this.props.id}`}></label>
                    <span className="itemLabel">{this.props.name}</span>
                </div>
            );
        }
    }
}
