﻿import * as React from "react";
import SettingSquare from "./SettingSquare";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { IFullTreeProps as FullTree, IBoxProps as BoxView, IModelProps as BoxEntityView } from "../interfaces/irules";
import { selectParent } from "../actions/mainActions";
import { createBox, deleteBox } from "../actions/editorActions";
import { actionConstants } from "../constants/actionConstants";
import { utilsConstants } from "../constants/utilsConstants";
import { routeConstants } from "../constants/routeConstants";
import CustomListView from "./multiselect/CustomListView";
import CustomTwoColumnsListView from "./multiselect/CustomTwoColumnsListView";
import CustomTwoColumnsTreeListView from "./multiselect/CustomTwoColumnsTreeListView";


interface IProps {
    fullTree: FullTree;
    companyGroups: BoxView[];
    organizations: BoxView[];
    units: BoxView[];
    roles: BoxView[];
    selectParent: (selectedValueId: string, route: string) => void;
    companyGroupEntities: BoxEntityView[];
    organizationEntities: BoxEntityView[];
    unitEntities: BoxEntityView[];
    roleEntities: BoxEntityView[];
    statusEntities: BoxEntityView[];
    allowedToManage: boolean;
    createBox: (ids: string[], parentId: string, accessGroupId: string, route: string, reducerRoute: string) => void;
    deleteBox: (currentId: string, accessGroupId: string, route: string, reducerRoute: string) => void;
    parentId: string;
}


class Settings extends React.Component<IProps, {}> {
    private companyGroupRef: React.RefObject<SettingSquare<{}>>;
    private organizationRef: React.RefObject<SettingSquare<{}>>;
    private unitRef: React.RefObject<SettingSquare<{}>>;
    private roleRef: React.RefObject<SettingSquare<{}>>;

    constructor(props) {
        super(props);
        this.companyGroupRef = React.createRef();
        this.organizationRef = React.createRef();
        this.unitRef = React.createRef();
        this.roleRef = React.createRef();
    }

    onSelectCompanyGroup(selectedItemId) {
        this.props.selectParent(selectedItemId, actionConstants.SELECT_COMPANY_GROUP_VALUE);
    }

    onSelectOrganization(selectedItemId) {
        this.props.selectParent(selectedItemId, actionConstants.SELECT_ORGANIZATION_VALUE);
    }

    onSelectUnit(selectedItemId) {
        this.props.selectParent(selectedItemId, actionConstants.SELECT_UNITS_VALUE);
    }

    onSelectRole(selectedItemId) {
        this.props.selectParent(selectedItemId, actionConstants.SELECT_ROLES_VALUE);
    }

    async createCompanyGroup(ids) {
        await this.props.createBox(ids, this.props.parentId, null, routeConstants.CREATE_COMPANY_GROUPS_URL, actionConstants.CREATE_COMPANY_GROUPS_URL);
        this.companyGroupRef.current.switchEditor();
    }

    async createOrganization(ids, parentId) {
        await this.props.createBox(ids, parentId, this.props.parentId, routeConstants.CREATE_ORGANIZATION_BOX_URL, actionConstants.CREATE_ORGANIZATION_BOX_URL);
        this.organizationRef.current.switchEditor();
    }

    async createUnit(ids, parentId) {
        await this.props.createBox(ids, parentId, this.props.parentId, routeConstants.CREATE_UNIT_BOX_URL, actionConstants.CREATE_UNIT_BOX_URL);
        this.unitRef.current.switchEditor();
    }

    async createRole(ids, parentId) {
        await this.props.createBox(ids, parentId, this.props.parentId, routeConstants.CREATE_ROLE_BOX_URL, actionConstants.CREATE_ROLE_BOX_URL);
        this.roleRef.current.switchEditor();
    }

    async createStatus(ids, parentId) {
        await this.props.createBox(ids, parentId, this.props.parentId, routeConstants.CREATE_STATUS_BOX_URL, actionConstants.CREATE_STATUS_BOX);
        this.roleRef.current.switchAdditionalEditor();
    }

    async deleteCompanyGroup(ids) {
        await this.props.deleteBox(ids, this.props.parentId, routeConstants.DELETE_COMPANY_GROUP_BOX_URL, actionConstants.DELETE_COMPANY_GROUPS_URL);
    }

    async deleteOrganization(ids) {
        await this.props.deleteBox(ids, this.props.parentId, routeConstants.DELETE_ORGANIZATION_BOX_URL, actionConstants.DELETE_ORGANIZATION_BOX_URL);
    }

    async deleteUnit(ids) {
        await this.props.deleteBox(ids, this.props.parentId, routeConstants.DELETE_UNIT_BOX_URL, actionConstants.DELETE_UNIT_BOX_URL);
    }

    async deleteRole(ids) {
        await this.props.deleteBox(ids, this.props.parentId, routeConstants.DELETE_ROLE_BOX_URL, actionConstants.DELETE_ROLE_BOX_URL);
    }

    render() {
        const companyGroupLabel = [{ id: utilsConstants.COMPANY_GROUP_SYS_CODE, name: "SubGroups", parentBoxId: null, selectedValueId: null}];
        const organizationLabel = [{ id: utilsConstants.UNIT_SYS_CODE, name: "Stacks", parentBoxId: null, selectedValueId: null }];
        const unitLabel = [{ id: utilsConstants.ROLE_SYS_CODE, name: "Units", parentBoxId: null, selectedValueId: null }];

        const fullBoxesList = companyGroupLabel.concat(this.props.companyGroups).concat(organizationLabel).concat(this.props.organizations).concat(unitLabel).concat(this.props.units);

        const companyGroupsEditor = <CustomListView source={this.props.companyGroupEntities} apply={this.createCompanyGroup.bind(this)} />;
        const organizationsEditor = <CustomTwoColumnsListView source={this.props.organizationEntities} parentSource={this.props.companyGroups} visibleSelectAllBtn={false} visibleUnselectAllBtn={false} apply={this.createOrganization.bind(this)} />;
        const unitsEditor = <CustomTwoColumnsTreeListView source={this.props.unitEntities} parentSource={this.props.organizations} apply={this.createUnit.bind(this)} />;
        const rolesEditor = <CustomTwoColumnsListView source={this.props.roleEntities} parentSource={fullBoxesList} visibleSelectAllBtn={false} visibleUnselectAllBtn={false} apply={this.createRole.bind(this)} />;
        const statusEditor = <CustomTwoColumnsListView source={this.props.statusEntities} parentSource={fullBoxesList} visibleSelectAllBtn={false} visibleUnselectAllBtn={false} apply={this.createStatus.bind(this)} />;

        return (
            <div className="mainSettingsBox">
                <div className="row">
                    <div className="col-sm mainSettingsSquareLabel">Main settings</div>
                </div>
                <div className="row mb-60">
                    <div className="col-sm">
                        <SettingSquare ref={this.companyGroupRef} disabled={this.props.allowedToManage} source={
                            this.props.companyGroups} groupName="CompanyGroups" editor={companyGroupsEditor} onSelect={this.onSelectCompanyGroup.bind(this)} onDelete={this.deleteCompanyGroup.bind(this)}
                            squareLabel="SubGroups" btnLabel="+Add new SubGroup" />
                    </div>
                    <div className="col-sm">
                        <SettingSquare ref={this.organizationRef} disabled={this.props.allowedToManage} source={
                            this.props.organizations} groupName="Organizations" editor={organizationsEditor} itemStructureBtn={true} onSelect={this.onSelectOrganization.bind(this)} onDelete={this.deleteOrganization.bind(this)}
                            squareLabel="Stacks" btnLabel="+Add new Stack" />
                    </div>
                </div>
                <div className="row mb-60">
                    <div className="col-sm">
                        <SettingSquare ref={this.unitRef} disabled={this.props.allowedToManage} source={
                            this.props.units} groupName="Units" editor={unitsEditor} itemStructureBtn={true} onSelect={this.onSelectUnit.bind(this)} onDelete={this.deleteUnit.bind(this)} squareLabel="Units" btnLabel="+Add new Unit" />
                    </div>
                    <div className="col-sm">
                        <SettingSquare ref={this.roleRef} disabled={this.props.allowedToManage} source={
                            this.props.roles} groupName="Roles" editor={rolesEditor} itemStructureBtn={true} additionalBtn={true} additionalBtnLabel="+Add new Status" additionalEditor={
                                statusEditor} onSelect={this.onSelectRole.bind(this)}
                            onDelete={this.deleteRole.bind(this)}
                            squareLabel="Roles/Statuses" btnLabel="+Add new Role" />
                    </div>
                </div>
            </div>
        );
    }
}

let mapProps = (state) => {
    return {
        fullTree: state.mainReducer.fullTree,
        companyGroups: state.mainReducer.companyGroups,
        organizations: state.mainReducer.organizations,
        units: state.mainReducer.units,
        roles: state.mainReducer.roles,
        companyGroupEntities: state.editorReducer.companyGroupEntities,
        organizationEntities: state.editorReducer.organizationEntities,
        unitEntities: state.editorReducer.unitEntities,
        roleEntities: state.editorReducer.roleEntities,
        statusEntities: state.editorReducer.statusEntities,
        allowedToManage: state.editorReducer.allowedToManage,
        parentId: state.mainReducer.parentId,
        error: state.mainReducer.error
    };
};

let mapDispatch = (dispatch) => {
    return {
        selectParent: bindActionCreators(selectParent, dispatch),
        createBox: bindActionCreators(createBox, dispatch),
        deleteBox: bindActionCreators(deleteBox, dispatch)
    };
};

export default connect(mapProps, mapDispatch)(Settings);