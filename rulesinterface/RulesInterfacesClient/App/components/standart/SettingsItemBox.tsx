﻿import * as React from 'react';
import { connect } from "react-redux";
import { IBoxProps as BoxView, IFullTreeProps as FullTree } from "../../interfaces/irules";

interface IProps<T> {
    groupName: string;
    label: string;
    value: string;
    status: boolean;
    structureBtn: boolean;
    select: (id: string, checked: boolean) => void;
    delete: (id: string) => void;
    checked: boolean;
    statuses: BoxView[];
    parentId: string;
    fullTree: FullTree;
}

interface IState {
}

class SettingsItemBox<T> extends React.Component<IProps<T>, IState> {
    constructor(props) {
        super(props);
        this.state = { openStatus: false };
    }

    select() {
        this.props.select(this.props.value, this.props.checked);
    }

    delete() {
        this.props.delete(this.props.value);
    }

    render() {
        let statusLabel = [];
        if (this.props.value) {
            statusLabel = this.props.statuses.filter(f => f.parentBoxId === this.props.value) ? this.props.statuses.filter(f => f.parentBoxId === this.props.value).map(m => {
                return (
                    <div key={m.name} className="statusInfoItem">{m.name}</div>
                );
            }) : [];
        }

        let parentsource = this.props.fullTree.companyGroups.concat(this.props.fullTree.organizations).concat(this.props.fullTree.units);
        let parentBoxFirstLayer = parentsource.filter(f => f.id === this.props.parentId)[0];
        let parentBoxSecondLayer;
        if (parentBoxFirstLayer) {
            parentBoxSecondLayer = parentsource.filter(f => f.id === parentBoxFirstLayer.parentBoxId)[0];
        }
        let parentBoxThirdLayer;
        if (parentBoxSecondLayer) {
            parentBoxThirdLayer = parentsource.filter(f => f.id === parentBoxSecondLayer.ParentBoxId)[0];
        }

        let secondLayer;
        if (parentBoxSecondLayer) {
            secondLayer = <div className="row">
                                  <div className="col-sm-1"><i className="structure-info-ic"></i></div>
                                  <div className="col-sm">{parentBoxSecondLayer ? parentBoxSecondLayer.Name : null
                                  }</div>
                              </div>;
        }

        let thirdLayer
        if (parentBoxThirdLayer) {
            thirdLayer = <div className="row">
                                 <div className="col-sm-1"><i className="structure-point-ic"></i></div>
                                 <div className="col-sm">{parentBoxThirdLayer ? parentBoxThirdLayer.Name : null}</div>
                             </div>;
        }

        //Если необходимо добавить доп. (i) кнопку - раскомментировать.
        //let infoBtn = <div className="helpInformationBtn">i</div>;

        let statusInfo = <div className="statusInfo">{statusLabel}</div>;
        let structureInfo =
            <div className="structureInfo">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm p-3 structureInfoLabel">Структура компании</div>
                    </div>
                    <div className="row">
                        <div className="col-sm-1 mt-3"><i className="structure-info-ic"></i></div>
                        <div className="col-sm p-3">{parentBoxFirstLayer ? parentBoxFirstLayer.name : null}</div>
                    </div>
                    {secondLayer}
                    {thirdLayer}
                </div>
            </div>;

        let status = <div className="statusBtn">{statusInfo}{`Статусы (${statusLabel.length})`}</div>;
        let deleteBtn = this.props.checked ? <div className="deleteBtnItem" onClick={this.delete.bind(this)}><i className="item-delete-ic"></i></div> : null;
        let structureBtn = this.props.structureBtn ? <div className="structureBtn">{structureInfo}<i className="structure-ic"></i></div> : null;
        let color = this.props.checked ? "redLabel" : null;

        return (
            <div className="accessItem" onClick={this.select.bind(this)}>
                <span className={color}>{this.props.label}</span>
                {deleteBtn}
                {status}
                {structureBtn}
            </div>
        );
    }
}

let mapProps = (state) => {
    return {
        statuses: state.mainReducer.statuses,
        fullTree: state.mainReducer.fullTree
    };
};

let mapDispatch = (dispatch) => {
    return {
    };
};

export default connect(mapProps, mapDispatch)(SettingsItemBox);