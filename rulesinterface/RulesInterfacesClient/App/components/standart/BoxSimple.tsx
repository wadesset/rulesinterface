﻿import * as React from 'react';

interface IProps {
    label: string;
    value: string;
    openSettings: (value: string)=>void;
}

export default class BoxSimple extends React.Component<IProps, {}> {
    openSettings() {
        this.props.openSettings(this.props.value);
    }

    render() {
        return (
            <div className="accessItem">
                <i className="menu-item-icon dot-ic"></i>
                <span>{this.props.label}</span>
                <div className="settingsBtn" onClick={this.openSettings.bind(this)}><i className="settings-wheel-ic"></i></div>
            </div>
        );
    }
}