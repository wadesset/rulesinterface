﻿import * as React from 'react';
import { IBoxProps as BoxView } from "../interfaces/irules";
import SettingsItemBox from "./standart/SettingsItemBox";


interface IProps<T> {
    source: BoxView[];
    squareLabel: string;
    btnLabel: string;
    additionalBtn: boolean;
    additionalBtnLabel: string;
    itemStructureBtn: boolean;
    onSelect: (selectedValueId: string) => void;
    onDelete: (currentBoxId: string) => void;
    editor: T;
    additionalEditor: T;
    disabled: boolean;
    groupName: string;
}

interface IState {
    switchEditor: boolean;
    switchAdditionalEditor: boolean;
    checkedId: string;
}

export default class SettingSquare<T> extends React.Component<IProps<T>, IState> {
    constructor(props) {
        super(props);
        this.state = { switchEditor: false, switchAdditionalEditor: false, checkedId: null };
    }

    static defaultProps = {
        additionalBtnLabel: "",
        itemStructureBtn: false,
    } as Partial<IProps<{}>>;

    //Удаляет дубликаты// так себе конечно...
    removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    }

    onSelect(selectedItemId, alreadyChecked) {
        if (alreadyChecked) {
            this.props.onSelect(null);
            this.setState({ checkedId: null });   
        } else {
            this.props.onSelect(selectedItemId);
            this.setState({ checkedId: selectedItemId });    
        }
    }

    onDelete(currentBoxId) {
        this.props.onDelete(currentBoxId);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.source === nextProps.source && nextState === this.state.switchEditor) {
            return false;
        }

        return true;
    }

    public switchEditor() {
        if (this.state.switchAdditionalEditor) {
            this.setState({ switchAdditionalEditor: false }, () => { this.setState({ switchEditor: !this.state.switchEditor }) });
        } else {
            this.setState({ switchEditor: !this.state.switchEditor });
        }
    }

    public switchAdditionalEditor() {
        if (this.state.switchEditor) {
            this.setState({ switchEditor: false }, () => { this.setState({ switchAdditionalEditor: !this.state.switchAdditionalEditor }) });
        } else {
            this.setState({ switchAdditionalEditor: !this.state.switchAdditionalEditor });
        }
    }

    render() {
        let editor = <div className="settings-square-editor">{this.props.editor}</div>;
        let additionalEditor = <div className="settings-square-editor">{this.props.additionalEditor}</div>;
        let sourceList = this.props.source.map(item => {
            let background = this.props.source.indexOf(item) % 2 === 0 ? "whiteItem" : "transparentItem";
            let checked = this.state.checkedId === item.id;
            return (
                <div key={item.id} className={background}>
                    <SettingsItemBox checked={checked} groupName={this.props.groupName} label={item.name} parentId={item.parentBoxId} structureBtn={this.props.itemStructureBtn} select={() => { this.onSelect.bind(this)(item.id, checked) }} delete={this.onDelete.bind(this)} value={item.id} status={true} />
                </div>
            );
        });

        let viewer = <div className="settings-square">{sourceList}</div>;
        let switchEditor = this.state.switchEditor ? editor : viewer;
        let additionalSwitchEditor = this.state.switchAdditionalEditor ? additionalEditor : switchEditor;

        let additionalBtn = this.props.additionalBtn
            ? <div className="col-sm ta-r">
                <input className="addSettingsItem" disabled={this.props.disabled} type="button" onClick={this.switchAdditionalEditor.bind(this)} value={this.props.additionalBtnLabel} />
            </div>
            : null;


        return (
            <div className="wd-100 p-r-30">
                <div className="row mb-3">
                    <div className="col-sm settingsSquareLabel">{this.props.squareLabel}</div>
                    {additionalBtn}
                    <div className="col-sm ta-r">
                        <input className="addSettingsItem" disabled={this.props.disabled} type="button" onClick={this.switchEditor.bind(this)} value={this.props.btnLabel} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        {additionalSwitchEditor}
                    </div>
                </div>
            </div>
        );
    }
}