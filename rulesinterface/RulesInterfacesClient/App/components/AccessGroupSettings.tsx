﻿import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { utilsConstants } from "../constants/utilsConstants"
import { actionConstants} from "../constants/actionConstants"
import { routeConstants } from "../constants/routeConstants"
import { openSettings, updateBox, deleteBox } from "../actions/editorActions";
import { updateAccessGroupName } from "../actions/mainActions";
import { IBoxProps as BoxView, IModelProps as BoxEntityView } from "../interfaces/irules";
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'


interface IProps {
    accessGroupEntities: BoxEntityView[];
    openSettings: (value: boolean) => void;
    updateBox: (selectedItem: string, boxId: string) => void;
    deleteBox: (currentBoxId: string, accessGroupId: string, route: string, reducerRoute: string) => void;
    updateAccessGroupName: (id: string, value: string) => void;
    parentItem: BoxView;
}


class AccessGroupSettings extends React.Component<IProps, {}> {
    constructor(props) {
        super(props);
    }

    onChange() {
        this.props.openSettings(false);
    }

    onSelect(selectedItem) {
        this.props.updateBox(selectedItem.value, this.props.parentItem.id);
    }

    onChangeName(event) {
        this.props.updateAccessGroupName(this.props.parentItem.id, event.target.value);
    }

    onDelete() {
        this.props.deleteBox(this.props.parentItem.id, null, routeConstants.DELETE_ACCESS_GROUP_BOX_URL, actionConstants.DELETE_ACCESS_GROUPS);
        this.props.openSettings(false);
    }

    render() {
        const source = this.props.accessGroupEntities.map(item => {
            return { value: item.id, label: item.name }
        });
        let defaultValue = this.props.parentItem.selectedValueId === utilsConstants.DEFAULT_GUID
            ? "Choose Group"
            : this.props.parentItem.selectedValueId;

        return (
            <div className="mainSettingsBox">
                <div className="row">
                    <div className="col-sm-1">
                        <span className="mainSettingsSquareLabel">Settings</span>
                    </div>
                    <div className="col-sm-2">
                        <button className="returnBtn ml-5" onClick={this.onChange.bind(this)}><i className="back-ic"></i><span className="ml-2">Back</span></button>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-sm-2">
                        <span>Name</span>
                        <input className="settingsInput mt-1" placeholder="Enter name" type="text" onChange={this.onChangeName.bind(this)} value={this.props.parentItem.name} />
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-sm-3">
                        <span>Choose Group</span>
                        <Dropdown className="mt-1" options={source} value={defaultValue} onChange={this.onSelect.bind(this)} placeholder="Choose Group" />
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-sm-3">
                        <span>Description</span>
                        <div className="mt-1">
                            <textarea className="settingsTextArea" placeholder="Enter description"></textarea></div>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-sm-2">
                        <span>Owner</span>
                        <input className="settingsInput mt-1" placeholder="Find owner" type="text" />
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-2">
                        <span>Group type</span>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-sm-2">
                        <div className="groupTypeBtnWhite">Active Directory</div>
                    </div>
                    <div className="col-sm-2">
                        <div className="groupTypeBtnRed">Share Point</div>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-2">
                        <span>Activities</span>
                        <div className="">
                            <div className="button r" id="button-9">
                                <input type="checkbox" className="checkbox" />
                                <div className="knobs">
                                    <span></span>
                                </div>
                                <div className="layer"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-sm-2">
                        <div className="deleteAccessGroupBtn" onClick={this.onDelete.bind(this)}><i className="delete-ic"></i><span className="ml-2">Delete this group</span></div>
                    </div>
                </div>
            </div>
        );
    }
}

let mapProps = (state) => {
    return {
        parentItem: state.mainReducer.parentItem,
        accessGroupEntities: state.editorReducer.accessGroupEntities,
    };
};

let mapDispatch = (dispatch) => {
    return {
        openSettings: bindActionCreators(openSettings, dispatch),
        updateBox: bindActionCreators(updateBox, dispatch),
        deleteBox: bindActionCreators(deleteBox, dispatch),
        updateAccessGroupName: bindActionCreators(updateAccessGroupName, dispatch)
    };
};

export default connect(mapProps, mapDispatch)(AccessGroupSettings);