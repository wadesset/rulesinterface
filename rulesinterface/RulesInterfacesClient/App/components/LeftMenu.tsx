﻿import * as React from 'react';
import { getFullTree, getAccessGroups, setDefaultStates, setParentItem, createAccessGroup } from "../actions/mainActions";
import { IBoxProps as BoxView, IFullTreeProps as FullTree } from "../interfaces/irules";
import BoxSimple from "../components/standart/BoxSimple";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getAccessGroupEntities, getCompanyGroupEntities, getOrganizationEntities, getUnitEntities, getRoleEntities, getStatusEntities, allowedToManage, openSettings } from "../actions/editorActions";

interface IProps {
    setDefaultStates: () => void;
    getAccessGroups: () => void;
    createAccessGroup: () => void;
    getFullTree: (parentId: string) => void;
    getAccessGroupEntities: () => void;
    getCompanyGroupEntities: () => void;
    getOrganizationEntities: () => void;
    getUnitEntities: () => void;
    getRoleEntities: () => void;
    getStatusEntities: () => void;
    allowedToManage: (value: boolean) => void;
    openSettings: (value: boolean) => void;
    setParentItem: (parentId: string) => void;
    fullTree: FullTree;
    accessGroups: BoxView[];
}

class LeftMenu extends React.Component<IProps, {}> {
    constructor(props) {
        super(props);
        this.props.getAccessGroups();
        this.props.getAccessGroupEntities();
        this.props.getCompanyGroupEntities();
        this.props.getOrganizationEntities();
        this.props.getUnitEntities();
        this.props.getRoleEntities();
        this.props.getStatusEntities();
    }

    async getFullTree(parentId) {
        await this.props.getFullTree(parentId);
        await this.props.setDefaultStates();
        await this.props.allowedToManage(false);
        await this.props.setParentItem(parentId);
    }

    createAccessGroup() {
        this.props.createAccessGroup();
    }

    openSettings(value) {
        this.props.openSettings(true);
    }

    render() {
        let accessList = this.props.accessGroups ? this.props.accessGroups.map(item => {
            return (
                <div key={item.id} className="menu-item">
                    <input className="display-n" type="radio" name="menu" id={item.id} />
                    <label className="w-100" key={item.id} htmlFor={item.id} onClick={() => this.getFullTree.bind(this)(item.id)}>
                        <BoxSimple label={item.name} value={item.id} openSettings={this.openSettings.bind(this)} />
                    </label>
                </div>
            );
        }) : [];


        return (
            <div className="leftScrollMenuBox min-wd-235">
                <div className="redThing"></div>
                <div className="row leftScrollMenuSubBox">
                    <div className="col-sm">
                        <div className="row">
                            <div className="col-sm mainSettingsSquareLabel">Groups</div>
                        </div>
                        <div className="row">
                            <div className="col-sm">
                                <input className="addAccessBtn" type="button" value="+ Add new group" onClick={this.createAccessGroup.bind(this)} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm">
                                {accessList}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

let mapProps = (state) => {
    return {
        accessGroups: state.mainReducer.accessGroups,
        fullTree: state.mainReducer.fullTree,
        error: state.mainReducer.error
    };
};

let mapDispatch = (dispatch) => {
    return {
        getAccessGroups: bindActionCreators(getAccessGroups, dispatch),
        getFullTree: bindActionCreators(getFullTree, dispatch),
        setDefaultStates: bindActionCreators(setDefaultStates, dispatch),
        getAccessGroupEntities: bindActionCreators(getAccessGroupEntities, dispatch),
        getCompanyGroupEntities: bindActionCreators(getCompanyGroupEntities, dispatch),
        getOrganizationEntities: bindActionCreators(getOrganizationEntities, dispatch),
        getUnitEntities: bindActionCreators(getUnitEntities, dispatch),
        getRoleEntities: bindActionCreators(getRoleEntities, dispatch),
        getStatusEntities: bindActionCreators(getStatusEntities, dispatch),
        allowedToManage: bindActionCreators(allowedToManage, dispatch),
        openSettings: bindActionCreators(openSettings, dispatch),
        setParentItem: bindActionCreators(setParentItem, dispatch),
        createAccessGroup: bindActionCreators(createAccessGroup, dispatch)
    };
};

export default connect(mapProps, mapDispatch)(LeftMenu);