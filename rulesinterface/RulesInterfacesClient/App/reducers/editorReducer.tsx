import { actionConstants } from "../constants/actionConstants";
import { IModelProps as BoxEntityView} from "../interfaces/irules";

const initialState = {
    component: null,
    accessGroupEntities: [] as BoxEntityView[],
    companyGroupEntities: [] as BoxEntityView[],
    organizationEntities: [] as BoxEntityView[],
    unitEntities: [] as BoxEntityView[],
    roleEntities: [] as BoxEntityView[],
    statusEntities: [] as BoxEntityView[],
    allowedToManage: true,
    openedSettings: false,
    error: ''
};


export default function editorReducer(state = initialState, action) {
    switch (action.type) {
        case actionConstants.SWITCH_COMPANY_GROUPS_EDITOR:
            return { ...state, switchCompanyGroupsEditor: action.data, error: '' };

        case actionConstants.SWITCH_ORGANIZATIONS_EDITOR:
            return { ...state, switchCompanyGroupsEditor: action.data, error: '' };

        case actionConstants.SWITCH_UNITS_EDITOR:
            return { ...state, switchCompanyGroupsEditor: action.data, error: '' };

        case actionConstants.SWITCH_ROLES_EDITOR:
            return { ...state, switchCompanyGroupsEditor: action.data, error: '' };

        case actionConstants.GET_ACCESS_GROUP_ENTITY:
            return { ...state, accessGroupEntities: action.data, error: '' };

        case actionConstants.GET_COMPANY_GROUP_ENTITY:
            return { ...state, companyGroupEntities: action.data, error: '' };

        case actionConstants.GET_ORGANIZATION_ENTITY:
            return { ...state, organizationEntities: action.data, error: '' };

        case actionConstants.GET_UNIT_ENTITY:
            return { ...state, unitEntities: action.data, error: '' };

        case actionConstants.GET_ROLE_ENTITY:
            return { ...state, roleEntities: action.data, error: '' };

        case actionConstants.GET_STATUS_ENTITY:
            return { ...state, statusEntities: action.data, error: '' };

        case actionConstants.ALLOWED_TO_MANAGE:
            return { ...state, allowedToManage: action.data, error: '' };

        case actionConstants.OPEN_SETTINGS:
            return { ...state, openedSettings: action.data, error: '' };

        default:
            return state;
    }
}