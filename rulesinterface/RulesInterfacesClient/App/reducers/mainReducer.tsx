import { actionConstants } from "../constants/actionConstants";
import { IFullTreeProps as FullTree } from "../interfaces/irules";

const initialState = {
    accessGroups: [],
    fullTree: null as FullTree,
    companyGroups: [],
    organizations: [],
    units: [],
    roles: [],
    statuses: [],
    parentItem: null,
    parentId: null,
    error: ''
};

export default function mainReducer(state = initialState, action) {
    switch (action.type) {
        case actionConstants.GET_ACCESS_GROUP_BOXES:
            return { ...state, accessGroups: action.accessGroups, error: '' };

        case actionConstants.CREATE_ACCESS_GROUP_BOX:
            return { ...state, accessGroups: action.accessGroups, error: '' };

        case actionConstants.DEFAULT_STATE:
            return {
                ...state,
                companyGroups: state.fullTree ? state.fullTree.companyGroups : [],
                organizations: state.fullTree ? state.fullTree.organizations : [],
                units: state.fullTree ? state.fullTree.units : [],
                roles: state.fullTree ? state.fullTree.roles : [],
                statuses: state.fullTree ? state.fullTree.statuses : [],
                error: ''
            };

        case actionConstants.SELECT_COMPANY_GROUP_VALUE:
            return {
                ...state,
                organizations: state.fullTree.organizations.filter(f => f.parentBoxId === action.id),
                units: state.fullTree.units.filter(f => state.fullTree.organizations.filter(f => f.parentBoxId === action.id).map(m => m.id).includes(f.parentBoxId)),
                roles: state.fullTree.roles.filter(f => state.fullTree.units.filter(f => state.fullTree.organizations.filter(f => f.parentBoxId === action.id).map(m => m.id).includes(f.parentBoxId)).map(m => m.id).includes(f.parentBoxId)
                    || state.fullTree.organizations.filter(f => f.parentBoxId === action.id).map(m => m.id).includes(f.parentBoxId)
                    || f.parentBoxId === action.id),
                error: ''
            };

        case actionConstants.SELECT_ORGANIZATION_VALUE:
            return {
                ...state,
                units: state.fullTree.units.filter(f => f.parentBoxId === action.id),
                roles: state.fullTree.roles.filter(f => state.fullTree.units.filter(f => f.parentBoxId === action.id).map(m => m.id).includes(f.parentBoxId) || f.parentBoxId === action.id),
                error: ''
            };

        case actionConstants.SELECT_UNITS_VALUE:
            return {
                ...state,
                roles: state.fullTree.roles.filter(f => f.parentBoxId === action.id),
                error: ''
            };

        case actionConstants.GET_FULL_TREE:
            return { ...state, fullTree: action.data, error: '' };

        case actionConstants.SET_PARENT_ITEM:
            return { ...state, parentId: action.data, parentItem: state.accessGroups.filter(f => f.id === action.data)[0], error: '' };

        case actionConstants.UPDATE_ACCESS_GROUP_BOX:
            return { ...state, accessGroups: action.data, error: '' };

        case actionConstants.CREATE_COMPANY_GROUPS_URL:
            return { ...state, companyGroups: action.data.companyGroups, fullTree: action.data, error: '' };

        case actionConstants.CREATE_ORGANIZATION_BOX_URL:
            return { ...state, organizations: action.data.organizations, fullTree: action.data, error: '' };

        case actionConstants.CREATE_UNIT_BOX_URL:
            return { ...state, units: action.data.units, fullTree: action.data, error: '' };

        case actionConstants.CREATE_ROLE_BOX_URL:
            return { ...state, roles: action.data.roles, fullTree: action.data, error: '' };

        case actionConstants.CREATE_STATUS_BOX:
            return { ...state, statuses: action.data.statuses, fullTree: action.data, error: '' };

        case actionConstants.DELETE_ACCESS_GROUPS:
            return { ...state, accessGroups: action.data, error: '' };

        case actionConstants.DELETE_COMPANY_GROUPS_URL:
            return { ...state, companyGroups: action.data.companyGroups, fullTree: action.data, error: '' };

        case actionConstants.DELETE_ORGANIZATION_BOX_URL:
            return { ...state, organizations: action.data.organizations, fullTree: action.data, error: '' };

        case actionConstants.DELETE_UNIT_BOX_URL:
            return { ...state, units: action.data.units, fullTree: action.data, error: '' };

        case actionConstants.DELETE_ROLE_BOX_URL:
            return { ...state, roles: action.data.roles, fullTree: action.data, error: '' };

        case actionConstants.UPDATE_NAME_ACCESS_GROUP_BOX:
            return { ...state, parentItem: action.data.filter(f => f.id === action.parentId)[0], parentId: action.data.filter(f => f.id === action.parentId)[0].Id, accessGroups: action.data, error: '' };

        default:
            return state;
    }
}